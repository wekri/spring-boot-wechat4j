# spring-boot-wechat4j

#### 项目介绍
spring-boot 微信公众号快速开发工具。

[微信公众平台技术文档](https://mp.weixin.qq.com/wiki)

#### 软件架构

软件架构说明
- token 定时刷新token。默认内存，支持自定义存储
- 微信服务
- 自定义菜单
- 个性化菜单
- 消息管理
    - 发送模板消息
- 微信网页开发
- 用户管理
    - 用户标签管理
    - 获取用户列表
    - 获取用户基本信息
- 账号管理
    - 带参数的二维码
    - 长连接转短连接
- 微信小店（开发中）
- 微信支付（开发中）

##### 微信SDK功能接入


#### 安装教程

1. 打包到本地仓库
```
mvn clean install -DskipTests=true
```
2. 引入依赖
```xml
<dependency>
	<groupId>com.wekri.spring.boot</groupId>
	<artifactId>spring-boot-wechat4j</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

#### 使用说明

1. 开发微信服务
    1. 新建一个controller，继承```BaseWechatController```
    2. 暴露一个接口，给微信服务
    3. 自定义一个token，用于配置微信服务时验证
    ```java
    @RestController
    public class WeChatController extends BaseWechatController{    
       @RequestMapping("/api/weChatService")
       public String weChatService(HttpServletRequest request) {
           logger.info("into weChatService");
           return service(request);
       }
       ...
       /**
       * 微信公众号后台-服务器配置中的令牌(Token)
       *
       * @return
       */
       @Override
       protected String getToken() {
           return "d3fd4cc0d1e641778c15bae37c0f3a1e";
       }
    }
    ```
2. 自定义菜单：MenuApi
3. 模板消息：TemplateMsgApi

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)