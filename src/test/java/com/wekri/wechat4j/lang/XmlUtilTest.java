package com.wekri.wechat4j.lang;

import com.wekri.wechat4j.api.marketing.bean.HbList;
import com.wekri.wechat4j.api.marketing.bean.RedPackInfo;
import com.wekri.wechat4j.api.request.WeChatRequest;
import com.wekri.wechat4j.api.response.TextResponse;
import com.wekri.wechat4j.util.XmlUtil;
import org.junit.Test;

/**
 * @author liuweiguo.
 */
public class XmlUtilTest {

    @Test
    public void m0() throws Exception {
        String xml = "<hblist>" +
                "<hbinfo>" +
                "<openid><![CDATA[odFFd1GPtqc-23PSWQ49ofvzO_do]]></openid>" +
                "<amount>200</amount>" +
                "<rcv_time><![CDATA[2019-07-16 18:20:25]]></rcv_time>" +
                "</hbinfo>" +
                "</hblist>";

        HbList hbinfo = XmlUtil.parseObject(xml, HbList.class);
        System.out.println(hbinfo);
    }

    @Test
    public void parseRedPack() throws Exception {
        String xmlStr = "<xml>\n" +
                "<return_code><![CDATA[SUCCESS]]></return_code>\n" +
                "<return_msg><![CDATA[OK]]></return_msg>\n" +
                "<result_code><![CDATA[SUCCESS]]></result_code>\n" +
                "<err_code><![CDATA[SUCCESS]]></err_code>\n" +
                "<err_code_des><![CDATA[OK]]></err_code_des>\n" +
                "<mch_billno><![CDATA[2019071618195005259022409664]]></mch_billno>\n" +
                "<mch_id><![CDATA[1532238391]]></mch_id>\n" +
                "<detail_id><![CDATA[1000041701201907163000149769487]]></detail_id>\n" +
                "<status><![CDATA[RECEIVED]]></status>\n" +
                "<send_type><![CDATA[API]]></send_type>\n" +
                "<hb_type><![CDATA[NORMAL]]></hb_type>\n" +
                "<total_num>1</total_num>\n" +
                "<total_amount>200</total_amount>\n" +
                "<send_time><![CDATA[2019-07-16 18:19:53]]></send_time>\n" +
                "<hblist>\n" +
                "<hbinfo>\n" +
                "<openid><![CDATA[odFFd1GPtqc-23PSWQ49ofvzO_do]]></openid>\n" +
                "<amount>200</amount>\n" +
                "<rcv_time><![CDATA[2019-07-16 18:20:25]]></rcv_time>\n" +
                "</hbinfo>" +
                "<hbinfo>" +
                "<openid><![CDATA[odFFd1GPtqc-23PSWQ49ofvzO_do]]></openid>" +
                "<amount>200</amount>" +
                "<rcv_time><![CDATA[2019-07-16 18:20:25]]></rcv_time>" +
                "</hbinfo>" +
                "</hblist>\n" +
                "</xml>";

        RedPackInfo request = XmlUtil.parseObject(xmlStr, RedPackInfo.class);
        System.out.println(request);
    }

    @Test
    public void parseObject() throws Exception {
        String xmlStr = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[FromUser]]></FromUserName><CreateTime>123456789</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[subscribe]]></Event></xml>";
        xmlStr = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[fromUser]]></FromUserName><CreateTime>123456789</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[LOCATION]]></Event><Latitude>23.137466</Latitude><Longitude>113.352425</Longitude><Precision>119.385040</Precision></xml>";
        xmlStr = "<xml><ToUserName><![CDATA[gh_7f083739789a]]></ToUserName><FromUserName><![CDATA[oia2TjuEGTNoeX76QEjQNrcURxG8]]></FromUserName><CreateTime>1395658920</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[TEMPLATESENDJOBFINISH]]></Event><MsgID>375818756783669249</MsgID><Status><![CDATA[success]]></Status></xml>";

        WeChatRequest request = XmlUtil.parseObject(xmlStr, WeChatRequest.class);
        System.out.println(request);
    }

    @Test
    public void toXmlStr() {
        TextResponse response = new TextResponse();
        response.setToUserName("toUser");
        response.setFromUserName("fromUser");
        response.setCreateTime(12345678);
        response.setMsgType("text");
        response.setContent("你好");

        String xml = XmlUtil.toXmlStr(response);
        System.out.println(xml);
    }

}