package com.wekri.wechat4j.api.user;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.user.bean.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuweiguo.
 */
public class UserApiTest {

    UserApi userApi;
    String token;

    @Before
    public void before() {
        userApi = new UserApi();
        token = "17_adk3k3RXn4Uag8Dw-akVCHOhioLfoONs3IhuU-sp_Rp8piAeGKep0qd4BN6HWiEL6lMeXYwLwRPwODZh4Ov7gH45a0c8PVDzVkl8JeHxpMLOWyhZeaGC45J5HgH0Bg172Ay1cVC3xvirtA9RZNIgAGAKYT";
    }

    @Test
    public void getUserInfo() throws Exception {
        String openId = "oA_sbv0jIZep0gzKVDL2tns0rqZA";
        UserInfo userInfo = userApi.getUserInfo(token, openId);
        System.out.println(userInfo);
    }

    @Test
    public void getUserInfoBatch() throws Exception {
        List<SimpleUser> list = new ArrayList<>();
        list.add(new SimpleUser("oA_sbv0jIZep0gzKVDL2tns0rqZA"));
        list.add(new SimpleUser("oA_sbv10hZ_Fj_GfkJB6-9iRKN9o"));
        list.add(new SimpleUser("oA_sbv-H4gjM5dAdaeOtVtI7TAxE"));
        list.add(new SimpleUser("oA_sbv-Jr0qOVrwcx-BtH1n9-ikA"));
        UserInfoList result = userApi.getUserInfoBatch(token, list);
        System.out.println(JSONObject.toJSONString(result));
    }

    @Test
    public void getUserList() throws Exception {
        UserList userList = userApi.getUserList(token, "");
        System.out.println("公众号人数：" + userList.getTotal());
        System.out.println(JSONObject.toJSONString(userList.getData()));
        while (StringUtils.isNotEmpty(userList.getNext_openid())) {
            userList = userApi.getUserList(token, userList.getNext_openid());
            System.out.println(JSONObject.toJSONString(userList.getData()));
        }
    }

    @Test
    public void changeOpenId() throws Exception {
        List<String> list = new ArrayList<>();
        list.add("asdasd");
        ChangeResult result = userApi.changeOpenId(token, "asd", list);
        System.out.println(JSONObject.toJSONString(result));
    }

}
