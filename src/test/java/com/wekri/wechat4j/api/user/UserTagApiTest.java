package com.wekri.wechat4j.api.user;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.user.bean.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author liuweiguo
 * @date 2019/4/24.
 */
public class UserTagApiTest {

    UserTagApi api = new UserTagApi();
    String token = "20_ZCJpM6mdCEOxANbwutuXD-0YQxY6-6LgLkcGFkNqunLtUbba5yV1Z-ZRp6zDelTtkKv86WsjwebuIFCyeIFXX1-ZLnzlXw8ufSeu9gn4yoFxEK1cuaGnjQ_JvOQKOSaAIACGK";

    @Test
    public void createTag() throws Exception {
        TagResult tag = api.createTag(token, "shop_assistant");
        System.out.println(JSONObject.toJSONString(tag));
    }

    @Test
    public void getTags() throws Exception {
        Tags tags = api.getTags(token);
        System.out.println(JSONObject.toJSONString(tags));
    }

    @Test
    public void updateTag() throws Exception {
        Tag tag = new Tag();
        tag.setId(101);
        tag.setName("shop_assistant");
        BaseMsg baseMsg = api.updateTag(token, tag);
        System.out.println(JSONObject.toJSONString(baseMsg));
    }

    @Test
    public void deleteTag() throws Exception {
        BaseMsg baseMsg = api.deleteTag(token, 101);
        System.out.println(JSONObject.toJSONString(baseMsg));
    }

    @Test
    public void getTagUser() throws Exception {
        TagUserResult tagUser = api.getTagUser(token, 100, null);
        System.out.println(JSONObject.toJSONString(tagUser));
    }

    @Test
    public void batchTagging() throws Exception {
        List<String> list = new ArrayList<>();
        list.add("odFFd1JymYS1Z9hCjM-3U22XZjBw");
        BaseMsg baseMsg = api.batchTagging(token, 100, list);
        System.out.println(JSONObject.toJSONString(baseMsg));
    }

    @Test
    public void batchUnTagging() throws Exception {
        List<String> list = new ArrayList<>();
        list.add("odFFd1JymYS1Z9hCjM-3U22XZjBw");
        BaseMsg baseMsg = api.batchUnTagging(token, 100, list);
        System.out.println(JSONObject.toJSONString(baseMsg));
    }

    @Test
    public void getUserTag() throws Exception {
        UserTagResult userTag = api.getUserTag(token, "odFFd1GPtqc-23PSWQ49ofvzO_do");
        System.out.println(JSONObject.toJSONString(userTag));
    }
}