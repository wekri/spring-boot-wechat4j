package com.wekri.wechat4j.api.response;

import com.wekri.wechat4j.api.message.CustomerMsgApi;
import com.wekri.wechat4j.api.request.WeChatRequest;
import com.wekri.wechat4j.api.response.bean.ArticleItem;
import com.wekri.wechat4j.api.response.bean.Articles;
import com.wekri.wechat4j.util.XmlUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuweiguo
 */
public class ResponseHelperTest {
    CustomerMsgApi customerMsgApi;
    WeChatRequest weChatRequest = null;
    String token;
    String postDataStr = "<xml><ToUserName><![CDATA[gh_804ba1c84bfe]]></ToUserName>\n" +
            "<FromUserName><![CDATA[odFFd1GPtqc-23PSWQ49ofvzO_do]]></FromUserName>\n" +
            "<CreateTime>1567072767</CreateTime>\n" +
            "<MsgType><![CDATA[event]]></MsgType>\n" +
            "<Event><![CDATA[VIEW]]></Event>\n" +
            "<EventKey><![CDATA[http://wekri.com/hel]]></EventKey>\n" +
            "<MenuId>520493189</MenuId>\n" +
            "</xml>";

    @Before
    public void before() {
        customerMsgApi = new CustomerMsgApi();
        weChatRequest = XmlUtil.parseObject(postDataStr, WeChatRequest.class);
        token = "24_3685IIcrCEYe0OF77dur6HluvGwFt3X5nIUaxfGuGshlevzDHwOmy7GX3u85AENPuJbKvYz6JiDKoMuoeP77Eor_W-_XPcFotaWoz-DRcdZZb-HvjJKR_bXkIC6TGokhNxweqgjtI37Tr95TZPHeAHALBT";
    }

    @Test
    public void buildImageResponse() throws Exception {
        ImageResponse response = ResponseHelper.buildImageResponse(weChatRequest, "cqWuDl6VCjEd02Yx1crogcRDgTkJuKBK1qqPUd_incM");
        String xmlStr = XmlUtil.toXmlStr(response);
        System.out.println(xmlStr);
    }

    @Test
    public void buildVoiceResponse() throws Exception {
        VoiceResponse response = ResponseHelper.buildVoiceResponse(weChatRequest, "cqWuDl6VCjEd02Yx1crogcRDgTkJuKBK1qqPUd_incM");
        String xmlStr = XmlUtil.toXmlStr(response);
        System.out.println(xmlStr);
    }

    @Test
    public void buildVideoResponse() throws Exception {
        VideoResponse response = ResponseHelper.buildVideoResponse(weChatRequest,
                "cqWuDl6VCjEd02Yx1crogcRDgTkJuKBK1qqPUd_incM", "tt", "desc");
        String xmlStr = XmlUtil.toXmlStr(response);
        System.out.println(xmlStr);
    }

    @Test
    public void buildMusicResponse() throws Exception {
        MusicResponse response = ResponseHelper.buildMusicResponse(weChatRequest,
                "tt", "desc", "url", "url", "mid");
        String xmlStr = XmlUtil.toXmlStr(response);
        System.out.println(xmlStr);
    }

    @Test
    public void buildNewsResponse() throws Exception {
        Articles art = new Articles();

        List<ArticleItem> item = new ArrayList<>();

        ArticleItem t = new ArticleItem();
        t.setTitle("tt");
        t.setDescription("desc");
        t.setPicUrl("picurl");
        t.setUrl("irl");
        item.add(t);
        art.setItem(item);
        NewsResponse response = ResponseHelper.buildNewsResponse(weChatRequest, art);
        String xmlStr = XmlUtil.toXmlStr(response);
        System.out.println(xmlStr);
    }


}