package com.wekri.wechat4j.api.oauth;

import com.wekri.wechat4j.api.oauth.bean.AuthInfo;
import org.junit.Before;
import org.junit.Test;

/**
 * @author liuweiguo.
 */
public class OAuthApiTest {
    OAuthApi oAuthApi;
    String appId = "wxf053e3c55656e407";
    String secret = "901416e9a827bb4bc5a8d2d5dd4a9f72";

    @Before
    public void setUp() throws Exception {
        oAuthApi = new OAuthApi();
    }

    @Test
    public void getAccessToken() throws Exception {
        String code = "071c9oyO0Xwh3c2LMpAO0JiFyO0c9oyT";
        AuthInfo accessToken = oAuthApi.getAccessToken(appId, secret, code);
        System.out.println(accessToken.getOpenid());
        System.out.println(accessToken.getAccess_token());
    }

    @Test
    public void refreshToken() throws Exception {
        String token = "";
        AuthInfo accessToken = oAuthApi.refreshToken(appId, token);
        System.out.println(accessToken.getOpenid());
        System.out.println(accessToken.getAccess_token());
    }

    @Test
    public void getUserInfo() throws Exception {
    }

}
