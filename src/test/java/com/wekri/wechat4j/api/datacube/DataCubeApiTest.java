package com.wekri.wechat4j.api.datacube;

import com.wekri.wechat4j.api.datacube.bean.InterfaceSummary;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author liuweiguo
 * @date 2019/9/26.
 */
public class DataCubeApiTest {

    DataCubeApi api;
    String token;

    @Before
    public void before(){
        api = new DataCubeApi();
        token = "25_P0JNN4YC4R2NoBe9G_HY_Xs0ikrrMFuyQ7Svx2Vkrzrlv0Nd97ZcUNHds942grOTLDFqZGhlf559qXV-FeZ1Ul38wqD5CfyBf4CZzWOlTDVnN6dRM-QMDjBfrt4IFVeABAWTJ";
    }

    @Test
    public void interfaceSummary() throws Exception {
        List<InterfaceSummary> list = api.interfaceSummary(token, "2019-09-24", "2019-09-25");
        System.out.println(list);
    }

    @Test
    public void interfaceSummaryHour() throws Exception {
        List<InterfaceSummary> list = api.interfaceSummaryHour(token, "2019-09-25", "2019-09-25");
        System.out.println(list);
    }

}