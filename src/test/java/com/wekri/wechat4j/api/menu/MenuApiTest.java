package com.wekri.wechat4j.api.menu;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.menu.bean.Button;
import com.wekri.wechat4j.api.menu.bean.ButtonType;
import com.wekri.wechat4j.api.menu.bean.Menu;
import com.wekri.wechat4j.api.menu.bean.MenuResult;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuweiguo.
 */
public class MenuApiTest {

    MenuApi menuApi;
    String accessToken;

    @Before
    public void setUp() throws Exception {
        menuApi = new MenuApi();
        accessToken = "15_a0LTd2LCxAgVnESZRnuzQq3W-C0eR2Bp6TuTkhiPCrLAfhp8Cwo8lxZNR-i44UlAEwg0kBGHWg2j0zJlUI_cmJY6PTDA2rg5zrUkQsbxQAMAu--DSgPuVFNoSLtx4aDwwiMumJCZ6d1W3nMsPSRhAIAEHG";
    }

    @Test
    public void createMenu() throws Exception {
        Menu menu = new Menu();
        List<Button> btList = new ArrayList<>();
        btList.add(new Button.ButtonBuilder()
                .type(ButtonType.view)
                .name("赚钱")
                .url("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf053e3c55656e407&redirect_uri=http://hsk.gs.9188.com/loanweb/lend/toVisitPage.go?menu=makeMoney&response_type=code&scope=snsapi_base&state=#wechat_redirect")
                .build());

        btList.add(new Button.ButtonBuilder()
                .type(ButtonType.view)
                .name("获取客户")
                .url("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf053e3c55656e407&redirect_uri=http://hsk.gs.9188.com/loanweb/lend/toVisitPage.go?menu=customerList&response_type=code&scope=snsapi_base&state=#wechat_redirect")
                .build());


//        btList.add(new Button.ButtonBuilder()
//                .name("test")
//                .sub_button(new Button.ButtonBuilder().name("click").type(ButtonType.click).key("V001").build())
//                .sub_button(new Button.ButtonBuilder().name("发送位置").type(ButtonType.location_select).key("rselfmenu_2_0").build())
//                .build());

        btList.add(new Button.ButtonBuilder()
                .name("扫码")
                .sub_button(new Button.ButtonBuilder().name("扫码带提示").type(ButtonType.scancode_waitmsg).key("rselfmenu_0_0").build())
                .sub_button(new Button.ButtonBuilder().name("扫码推事件").type(ButtonType.scancode_push).key("rselfmenu_0_1").build())
                .build());
//
//        btList.add(new Button.ButtonBuilder()
//                .name("发图")
//                .sub_button(new Button.ButtonBuilder().name("系统拍照发图").type(ButtonType.pic_sysphoto).key("rselfmenu_1_0").build())
//                .sub_button(new Button.ButtonBuilder().name("拍照或者相册发图").type(ButtonType.pic_photo_or_album).key("rselfmenu_1_1").build())
//                .sub_button(new Button.ButtonBuilder().name("微信相册发图").type(ButtonType.pic_weixin).key("rselfmenu_1_2").build())
//                .build());

        menu.setButton(btList);
        BaseMsg msg = menuApi.createMenu(menu, accessToken);
        System.out.println(msg);

    }

    @Test
    public void getMenu() throws Exception {
        MenuResult menu = menuApi.getMenu(accessToken);
        System.out.println(JSONObject.toJSONString(menu));
    }

    @Test
    public void deleteMenu() throws Exception {
        BaseMsg msg = menuApi.deleteMenu(accessToken);
        System.out.println(msg);
    }

}