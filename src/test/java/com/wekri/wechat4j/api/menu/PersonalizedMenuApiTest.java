package com.wekri.wechat4j.api.menu;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.menu.bean.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuweiguo
 * @date 2018/11/2.
 */
public class PersonalizedMenuApiTest {
    PersonalizedMenuApi api;
    String accessToken;

    @Before
    public void setUp() throws Exception {
        api = new PersonalizedMenuApi();
        accessToken = "15_a0LTd2LCxAgVnESZRnuzQq3W-C0eR2Bp6TuTkhiPCrLAfhp8Cwo8lxZNR-i44UlAEwg0kBGHWg2j0zJlUI_cmJY6PTDA2rg5zrUkQsbxQAMAu--DSgPuVFNoSLtx4aDwwiMumJCZ6d1W3nMsPSRhAIAEHG";
    }

    @Test
    public void addConditional() throws Exception {
        Menu menu = new Menu();
        List<Button> buList = new ArrayList<>();
        buList.add(new Button.ButtonBuilder().name("哈哈").type(ButtonType.view).url("http://wekri.com").build());
        menu.setButton(buList);

        Matchrule matchrule = new Matchrule();
        matchrule.setCountry("法国");
        menu.setMatchrule(matchrule);
        ConditionalResult msg = api.addConditional(menu, accessToken);
        System.out.println(JSONObject.toJSONString(msg));
    }

    @Test
    public void deleteConditional() throws Exception {
        String menuId = "440154781";
        BaseMsg msg = api.deleteConditional(menuId, accessToken);
        System.out.println(msg);
    }

    @Test
    public void tryMatch() throws Exception {
        MenuResult match = api.tryMatch("oA_sbv6JsrRMLBaHs8yoo8h6K0gw", accessToken);
        System.out.println(JSONObject.toJSONString(match));
    }

}