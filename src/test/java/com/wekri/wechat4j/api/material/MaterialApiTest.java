package com.wekri.wechat4j.api.material;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.material.bean.*;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @author liuweiguo
 */
public class MaterialApiTest {

    MaterialApi api = null;
    String accessToken = null;

    @Before
    public void before() {
        api = new MaterialApi();
        accessToken = "24_vTsKQAm0KXCgh8ltTQKQObQkZIeXnyQFvM5wuKcwa45gEqC-uxA3V45Cdl32WRp9IeVz5B5eZDYkbNAqzNxl5QsCfkrkR3VsyHqeo8xMtYuWdWDIglbg8mEUb7HmmyBRdE2c6VTQ9DVBwrp-YJOjAFASDI";
    }

    @Test
    public void materialCount() throws Exception {
        MaterialCountResult result = api.materialCount(accessToken);
        System.out.println(result.isSuccess());
        System.out.println(result);
    }

    @Test
    public void materialList() throws Exception {
        MaterialListResult list = api.materialList(accessToken, MaterialType.image, 0, 20);
        System.out.println(JSONObject.toJSONString(list));
    }

    @Test
    public void getMaterial() throws Exception {
//        Content-Type: image/jpeg
        MaterialResult result = api.getMaterial(accessToken, "cqWuDl6VCjEd02Yx1crogaAQRbTKYcYzQo83G0uC98E");
        System.out.println(result.getResponseType());

        if (!result.getResponseType().contains("text")) {
            IOUtils.write(result.getBytes(), new FileOutputStream(new File("H://zzz.jpg")));
        } else {
            System.out.println(JSONObject.toJSONString(result));
        }
    }

    @Test
    public void addMaterial() throws Exception {
        AddMaterialResult res = api.addMaterial(accessToken, MaterialType.image, new File("C:\\Users\\A-0373\\Desktop\\1.jpg"));
        System.out.println(res);
    }

    @Test
    public void deleteMaterial() throws Exception {
        BaseMsg baseMsg = api.deleteMaterial(accessToken, "cqWuDl6VCjEd02Yx1crogaAQRbTKYcYzQo83G0uC98E");
        System.out.println(baseMsg);
    }

}