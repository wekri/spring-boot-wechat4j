package com.wekri.wechat4j.api.poi;

import com.alibaba.fastjson.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author liuweiguo
 * @date 2018/11/5.
 */
public class PoiApiTest {

    PoiApi api;
    String accessToken;

    @Before
    public void setUp() throws Exception {
        api = new PoiApi();
        accessToken = "15_DWoxEtN9MpxdSV5FRete6pqEW2DosswpAVoVRdDCdmPzTb8w7jkY4AyoEW7aXV7IxAGNWWhes8KZLDP9a2SoozPvfOE8Zjv13RUqMcLU8gqO0GEK97hcO2xWBXQB_-pq4Tjmgf-1vEhOv3hWQBBaAHAKVV";
    }

    @Test
    public void addPoi() throws Exception {
        BaseInfo baseInfo = new BaseInfo();
        baseInfo.setSid("33788392");
        baseInfo.setBusiness_name("一点点");
        baseInfo.setBranch_name("漕宝路店");
        baseInfo.setProvince("上海");
        baseInfo.setCity("上海");
        baseInfo.setDistrict("徐汇区");
        baseInfo.setTelephone("021-666666");
        baseInfo.setCategories("美食，奶茶，冷饮");
        baseInfo.setOffset_type("1");
        baseInfo.setLatitude("121.2912389");
        baseInfo.setLongitude("31.88778612");

        Poi poi = new Poi(new Business(baseInfo));
        PoiResult result = api.addPoi(poi, accessToken);
        System.out.println(JSONObject.toJSONString(result));
    }

    @Test
    public void getPoi() throws Exception {
        Poi poi = api.getPoi("", accessToken);
    }

}