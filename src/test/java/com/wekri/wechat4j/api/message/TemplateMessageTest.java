package com.wekri.wechat4j.api.message;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.message.bean.MiniProgram;
import com.wekri.wechat4j.api.message.bean.TemplateData;
import com.wekri.wechat4j.api.message.bean.TemplateMessage;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by liuweiguo.
 */
public class TemplateMessageTest {

    @Test
    public void build() {
        TemplateMessage message = new TemplateMessage();
        message.setTouser("openId");
        message.setTemplate_id("ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY");
        message.setUrl("http://weixin.qq.com/download");
        message.setMiniprogram(new MiniProgram("xiaochengxuappid12345", "index?foo=bar"));
        Map<String, TemplateData> map = new HashMap<String, TemplateData>();
        map.put("first", new TemplateData("恭喜你购买成功！", "#173177"));
        map.put("keyword1", new TemplateData("巧克力", "#173177"));
        map.put("keyword2", new TemplateData("39.8元", "#173177"));
        map.put("keyword3", new TemplateData("2014年9月22日", "#173177"));
        map.put("remark", new TemplateData("欢迎再次购买！", "#173177"));
        message.setData(map);

        System.out.println(JSONObject.toJSONString(message));

        TemplateMessage m = new TemplateMessage.MessageBuilder()
                .setTouser("openId")
                .setTemplate_id("ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY")
                .setUrl("http://weixin.qq.com/download")
                .setMiniprogram(new MiniProgram("xiaochengxuappid12345", "index?foo=bar"))
                .addTemplateData("first", new TemplateData("恭喜你购买成功！", "#173177"))
                .addTemplateData("keyword1", new TemplateData("巧克力", "#173177"))
                .addTemplateData("keyword2", new TemplateData("39.8元", "#173177"))
                .addTemplateData("keyword3", new TemplateData("2014年9月22日", "#173177"))
                .addTemplateData("remark", new TemplateData("欢迎再次购买！", "#173177"))
                .build();

        System.out.println(JSONObject.toJSONString(m));
    }

}