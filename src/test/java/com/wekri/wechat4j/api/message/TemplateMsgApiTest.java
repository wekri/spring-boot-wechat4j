package com.wekri.wechat4j.api.message;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.message.bean.TemplateData;
import com.wekri.wechat4j.api.message.bean.TemplateMessage;
import com.wekri.wechat4j.api.message.bean.TemplateMessageResult;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author liuweiguo
 * @date 2018/11/5.
 */
public class TemplateMsgApiTest {

    String accessToken;

    TemplateMsgApi api = new TemplateMsgApi();
    String id = "123";
    String openid = "oA_sbv5cg5sEj8WpMaGtnLAqQ0_o";
    String templateId = "drMGaq5FFiAo8wSrAMA2byQ_9W-xQA4Hg34n5FEsAbo";
    String appId = "wxf053e3c55656e407";
    String apiHost = "http://hsk.gs.9188.com";
    String title = "张先生急需8万元";
    String desc = "民营/私企员工、月打卡收入20000元、公积金已连续缴纳超过6个月";
    private static final String COLOR_BLUE = "#3BA8DF";

    @Before
    public void setUp() throws Exception {
        accessToken = "15_uGG0_T6J_-lO8n-7SAFSTUgt6w2wFYBtazbyI2LnGQ1rY8VUzPg0odlcOmmkLrqCtypiwvmbxBR15URyheOGOK71YTT_1nTQur9NOq3jjynLTnlUC2OuYHXoeesOTYBG8kRq50nyN7GQhnqPBHChAHAEKF";
    }

    @Test
    public void send() throws Exception {
        StringBuilder url = new StringBuilder("https://open.weixin.qq.com/connect/oauth2/authorize?appid=");

        url.append(appId)
                .append("&redirect_uri=")
                .append(apiHost)
                .append("/loanweb/lend/toVisitPage.go?menu=detail/")
                .append(id)
                .append("&response_type=code&scope=snsapi_base&state=#wechat_redirect");

        TemplateMessage templateMessage = new TemplateMessage.MessageBuilder()
                .setTouser(openid)
                .setTemplate_id(templateId)
                .setUrl(url.toString())
                .addTemplateData("first", new TemplateData("您在小鱼定制的新鲜订单出炉啦！！！"))
                .addTemplateData("tradeDateTime", new TemplateData(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date())))
                .addTemplateData("orderType", new TemplateData("定制订单"))
                .addTemplateData("customerInfo", new TemplateData("上海-徐汇" + "-" + title))
                .addTemplateData("orderItemName", new TemplateData("客户详情"))
                .addTemplateData("orderItemData", new TemplateData(desc + "\n"))
                .addTemplateData("remark", new TemplateData("点击“详情”立即查看！", COLOR_BLUE))
                .build();
        TemplateMessageResult result = api.send(templateMessage, accessToken);
        System.out.println(JSONObject.toJSONString(result));
    }

}