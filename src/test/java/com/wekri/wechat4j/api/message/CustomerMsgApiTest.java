package com.wekri.wechat4j.api.message;

import com.wekri.wechat4j.BaseMsg;
import org.junit.Before;
import org.junit.Test;

/**
 * @author liuweiguo
 * @date 2018/12/18.
 */
public class CustomerMsgApiTest {

    CustomerMsgApi api;
    String token;

    @Before
    public void setUp() throws Exception {
        api = new CustomerMsgApi();
        token = "24_3685IIcrCEYe0OF77dur6HluvGwFt3X5nIUaxfGuGshlevzDHwOmy7GX3u85AENPuJbKvYz6JiDKoMuoeP77Eor_W-_XPcFotaWoz-DRcdZZb-HvjJKR_bXkIC6TGokhNxweqgjtI37Tr95TZPHeAHALBT";
    }

    @Test
    public void sendText() throws Exception {
        String desc = "感谢您关注【有鱼客】服务平台！\n" +
                "\n" +
                "有鱼客自16年5月上线以来，累计与银行、消金等50多家金融机构达成合作，为20万+信贷员/办卡员/线上兼职提供获客、代理推广等服务。\n" +
                "\n" +
                "【金融赚客】——推荐好友赚佣金\n" +
                "<a href=\"https://loan.huishuaka.com/agency/#/makeMoney?show=gzgzh\">0门槛0投入，分享产品给好友，成为代理马上赚钱！</a>\n" +
                "\n" +
                "最新活动：\n" +
                "<a href=\"http://dkcms.youyuwo.com/a/activity/huodong/2018/1211/93.html?1545100035\">新户办卡送40元现金奖，办卡成功请【立即领取】</a>\n" +
                "\n" +
                "【信贷抢单】——极速获客\n" +
                "<a href=\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx916c3723305ebefa&redirect_uri=http://loan.huishuaka.com/loanweb/lend/toVisitPage.go?menu=customerList&response_type=code&scope=snsapi_base&state=#wechat_redirect\">一手用户，人工审核，按需定制，立即抢单赚钱！</a>\n" +
                "\n" +
                "现在认证最高送30元，点击底部菜单“抢单”立即认证吧！";
        BaseMsg msg = api.sendText(token, "oII-ewSOVMuP5gZ8TJxuMgQIEVVQ", desc);
        System.out.println(msg);
    }

    @Test
    public void sendImage() throws Exception {
        BaseMsg msg = api.sendImage(token, "odFFd1GPtqc-23PSWQ49ofvzO_do", "cqWuDl6VCjEd02Yx1crogcRDgTkJuKBK1qqPUd_incM");
        System.out.println(msg);
    }

    @Test
    public void sendVoice() throws Exception {
        BaseMsg msg = api.sendVoice(token, "odFFd1GPtqc-23PSWQ49ofvzO_do", "cqWuDl6VCjEd02Yx1crogcRDgTkJuKBK1qqPUd_incM");
        System.out.println(msg);
    }

    @Test
    public void sendVideo() throws Exception {
        BaseMsg msg = api.sendVideo(token, "odFFd1GPtqc-23PSWQ49ofvzO_do",
                "cqWuDl6VCjEd02Yx1crogcRDgTkJuKBK1qqPUd_incM",
                "title", "description");
        System.out.println(msg);
    }

    @Test
    public void sendMusic() throws Exception {

    }

    @Test
    public void sendNews() throws Exception {
        BaseMsg baseMsg = api.sendNews(token, "oHXQ4t4yvCPD28e9IfZOWvBil6KQ", "宝贝", "描述", "http://wekri.com", "http://wx.qlogo.cn/mmopen/g9RQicMD01M05uVyRteWIPT6AQU8DcA4qnaJJR0sbAtz6iaEYjhsjXbqL4FlSdng1RjeHXYe1R3l4BhtF150KhibGEy4EmpJrL0/64");
        System.out.println(baseMsg);
    }

    @Test
    public void sendMPNews() throws Exception {
    }

    @Test
    public void sendWXCard() throws Exception {
    }

}