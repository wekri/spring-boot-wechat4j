package com.wekri.wechat4j.api.marketing;

import com.wekri.wechat4j.api.marketing.bean.RedPackBean;
import com.wekri.wechat4j.api.marketing.bean.RedPackInfo;
import com.wekri.wechat4j.api.marketing.bean.RedPackResult;
import com.wekri.wechat4j.api.pay.MyConfig;
import com.wekri.wechat4j.api.pay.WeChatPayConfig;
import com.wekri.wechat4j.api.pay.WeChatPayRequest;
import com.wekri.wechat4j.api.pay.WeChatPayUtil;
import com.wekri.wechat4j.util.Cert;
import com.wekri.wechat4j.util.XmlUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

/**
 * @author liuweiguo
 * @date 2019/6/14.
 */
public class RedPackApiTest {
    WeChatPayConfig myConfig;
    RedPackApi api = new RedPackApi();
    RedPackBean bean = new RedPackBean();
    String xml = null;

    @Before
    public void before() throws Exception {
        myConfig = new MyConfig();

        bean.setMch_id("1532238391");
        bean.setWxappid("wxa84379b511a3448e");
        bean.setSend_name("童慧商贸");

        bean.setRe_openid("odFFd1GPtqc-23PSWQ49ofvzO_do");
        bean.setTotal_amount(100);
        bean.setTotal_num(1);
        bean.setMch_billno(api.generateBillNo());
        bean.setNonce_str(UUID.randomUUID().toString().replace("-", ""));

        bean.setWishing("卖出一盒艾乐");
        bean.setAct_name("销售返现活动");
        bean.setRemark("多卖多得");
        bean.setScene_id("PRODUCT_4");

        bean.setClient_ip("127.0.0.1");

        bean.setSign(WeChatPayUtil.generateSignature(bean, myConfig.getKey()));
        xml = XmlUtil.toXmlStr(bean);
        System.out.println(xml);
    }

    @Test
    public void send0() throws Exception {
        WeChatPayRequest request = new WeChatPayRequest(myConfig);
        String s = request.requestWithCert("/mmpaymkttransfers/sendredpack", xml, 6000, 6000);
        System.out.println(s);
    }

    @Test
    public void send() throws Exception {
        RedPackResult send = api.send(bean, new Cert(myConfig.getCertStream(), myConfig.getMchID()));
        System.out.println("下单结果：" + send);
    }

    @Test
    public void redPackInfo() throws Exception {
        String billNo = "2019071618390195759196887895";
        billNo = "2019071618195005259022409664";
        RedPackInfo packInfo = api.redPackInfo(billNo, new MyConfig());
        System.out.println(packInfo);
    }

}