package com.wekri.wechat4j.api.media;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.material.bean.MaterialResult;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author liuweiguo
 * @date 2018/11/5.
 */
public class MediaApiTest {

    MediaApi api;
    String accessToken;

    @Before
    public void setUp() throws Exception {
        api = new MediaApi();
        accessToken = "28_1s-9V-SvRUIDgyHkXm8vf3k1ntjJoCmEcFGjWOTJu7G3si0nYr457UyrMz-rdJeYZwxi2DLa5GgbSKoDhK6kwY0HFoda6EOn6erfr3SjmFIVMtdsYfD-BKmkRb0JPjWpPkZulUdUTsJiaQm9QTBbAHAILT";
    }

    @Test
    public void mediaUpload() throws Exception {
        MediaResult result = api.mediaUpload(new File("C:\\Users\\A-0373\\Desktop\\1.jpg"), accessToken);
        System.out.println(JSONObject.toJSONString(result));
        //{"success":true,"url":"http://mmbiz.qpic.cn/mmbiz_png/0sjPUVibvFoHJwE5PArGiaibQXFnCWlSTgGCluaaria0Q19ObYsYd8znc0Y2IiaahtkDefGGhiaHH5eiacJCR2jQ4fwMA/0"}
    }

    @Test
    public void mediaDownload() throws IOException {
        MaterialResult result = api.mediaDownload(accessToken, "6CeamUrC2e2eGEsxMdHBHOMsfyYIL7OCyXCxsZ6p7vL1Ji-ccPuuzRlKm1PIeMXy");
        System.out.println(result.getResponseType());
        IOUtils.write(result.getBytes(), new FileOutputStream(new File("E:\\a.jpg")));
    }

}