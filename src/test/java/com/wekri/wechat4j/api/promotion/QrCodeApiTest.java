package com.wekri.wechat4j.api.promotion;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.promotion.bean.QrCodeTicket;
import org.junit.Before;
import org.junit.Test;

/**
 * @author liuweiguo
 * @date 2018/11/6.
 */
public class QrCodeApiTest {

    QrCodeApi api;
    String accessToken;

    @Before
    public void setUp() throws Exception {
        api = new QrCodeApi();
        accessToken = "15_OyC37YODDwYGZ5YCR5d3kCws0jZTuQXNpaeXL9328tB1-hxghLwzndJ8PtKejsac_J76aIZT0kYlj8Zw-pxDjJmH2hScQToNy3ZC8iNgFynLQu6D-os0hFhTC08EWRfABATLG";
    }

    @Test
    public void createTemporaryQRCode() throws Exception {
        QrCodeTicket ticket = api.createTemporaryQRCode(1, 60, accessToken);
        System.out.println(JSONObject.toJSONString(ticket));
        System.out.println(ticket.showQrCode());
        ticket.downloadQrCode("E://qr.jpg");
    }

    @Test
    public void createTemporaryQRCodeStr() throws Exception {
        QrCodeTicket ticket = api.createTemporaryQRCodeStr("test", 60, accessToken);
        System.out.println(JSONObject.toJSONString(ticket));
        System.out.println(ticket.showQrCode());
    }

    @Test
    public void createPermanentQRCode() throws Exception {
        QrCodeTicket ticket = api.createPermanentQRCode(1, accessToken);
        System.out.println(JSONObject.toJSONString(ticket));
        System.out.println(ticket.showQrCode());
    }

    @Test
    public void createPermanentQRCodeStr() throws Exception {
        QrCodeTicket ticket = api.createPermanentQRCodeStr("test", accessToken);
        System.out.println(JSONObject.toJSONString(ticket));
        System.out.println(ticket.showQrCode());
    }

}