package com.wekri.wechat4j.api.promotion;

import com.wekri.wechat4j.api.promotion.bean.ShortUrl;
import org.junit.Test;

/**
 * @author liuweiguo
 * @date 2018/11/6.
 */
public class ShortUrlApiTest {

    ShortUrlApi api = new ShortUrlApi();
    String accessToken = "15_OyC37YODDwYGZ5YCR5d3kCws0jZTuQXNpaeXL9328tB1-hxghLwzndJ8PtKejsac_J76aIZT0kYlj8Zw-pxDjJmH2hScQToNy3ZC8iNgFynLQu6D-os0hFhTC08EWRfABATLG";

    @Test
    public void toShortUrl() throws Exception {
        ShortUrl shortUrl = api.toShortUrl("http://wekri.com", accessToken);
        System.out.println(shortUrl.getShort_url());
    }

}