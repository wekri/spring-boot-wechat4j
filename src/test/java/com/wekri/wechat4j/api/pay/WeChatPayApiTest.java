package com.wekri.wechat4j.api.pay;

import org.junit.Test;

import java.util.Map;

/**
 * @author liuweiguo
 * @date 2019/6/20.
 */
public class WeChatPayApiTest {
    @Test
    public void isPayResultNotifySignatureValid() throws Exception {
        WeChatPayApi api = new WeChatPayApi(new MyConfig());
        String postDataStr = "<xml><appid><![CDATA[wxa84379b511a3448e]]></appid>\n" +
                "<bank_type><![CDATA[CFT]]></bank_type>\n" +
                "<cash_fee><![CDATA[500]]></cash_fee>\n" +
                "<fee_type><![CDATA[CNY]]></fee_type>\n" +
                "<is_subscribe><![CDATA[N]]></is_subscribe>\n" +
                "<mch_id><![CDATA[1532238391]]></mch_id>\n" +
                "<nonce_str><![CDATA[33e4931e08474fc8841e433db1180f42]]></nonce_str>\n" +
                "<openid><![CDATA[odFFd1MEPNAs9QoIzBZ3DJKxG5_o]]></openid>\n" +
                "<out_trade_no><![CDATA[a8ea35223f6444ed9b0f66fd8ede4f0a]]></out_trade_no>\n" +
                "<result_code><![CDATA[SUCCESS]]></result_code>\n" +
                "<return_code><![CDATA[SUCCESS]]></return_code>\n" +
                "<sign><![CDATA[5941990A254C36D1534B80F9BBB2FC12AA3B5B9F4884A00F40B149E324BB6A17]]></sign>\n" +
                "<time_end><![CDATA[20190620172410]]></time_end>\n" +
                "<total_fee>500</total_fee>\n" +
                "<trade_type><![CDATA[JSAPI]]></trade_type>\n" +
                "<transaction_id><![CDATA[4200000296201906201663359237]]></transaction_id>\n" +
                "</xml>";
        Map<String, String> map = WeChatPayUtil.xmlToMap(postDataStr);

        boolean valid = api.payResultNotifySignatureValid(map, WeChatPayConstants.SignType.HMACSHA256);
        System.out.println(valid);
    }

}