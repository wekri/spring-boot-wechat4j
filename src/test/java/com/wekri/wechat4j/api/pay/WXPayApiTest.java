package com.wekri.wechat4j.api.pay;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liuweiguo
 * @date 2018/11/6.
 */
public class WXPayApiTest {

    WeChatPayApi api;

    @Before
    public void setUp() throws Exception {
        WeChatPayConfig config = new MyConfig();
        api = new WeChatPayApi(config);
    }

    @Test
    public void unifiedOrder() throws Exception {
        Map<String, String> data = new HashMap<>();
        data.put("body", "腾讯充值中心-QQ会员充值");
        data.put("out_trade_no", "2016090910595900000013");
        data.put("device_info", "");
        data.put("fee_type", "CNY");
        data.put("total_fee", "1");
        data.put("spbill_create_ip", "123.12.12.123");
        data.put("notify_url", "http://www.example.com/wxpay/notify");
        data.put("trade_type", "JSAPI");  // 此处指定为扫码支付
        data.put("product_id", "12");
        data.put("openid", "odFFd1GPtqc-23PSWQ49ofvzO_do");

        Map<String, String> map = api.unifiedOrder(data);
        System.out.println(map);

        //再签名

        Map<String, String> m = new HashMap<>();
        m.put("appId", map.get("appid"));
        m.put("timeStamp", System.currentTimeMillis()+"");
        m.put("nonceStr", map.get("nonce_str"));
        m.put("package", "prepay_id=" + map.get("prepay_id"));
        m.put("signType", "HMAC-SHA256");
        String sign = WeChatPayUtil.generateSignature(m, "a04d9505ec0342bf9dd41a53d757fc3f", WeChatPayConstants.SignType.HMACSHA256);
        m.put("paySign", sign);
        System.out.println(m);
    }

    @Test
    public void orderQuery() throws Exception {
        Map<String, String> reqData = new HashMap<>();
        Map<String, String> map = api.orderQuery(reqData);
        System.out.println(map);
    }

    @Test
    public void refundQuery() throws Exception {
        Map<String, String> reqData = null;
        api.refundQuery(reqData);
    }

    @Test
    public void downloadBill() throws Exception {
        Map<String, String> reqData = null;
        api.downloadBill(reqData);
    }

}