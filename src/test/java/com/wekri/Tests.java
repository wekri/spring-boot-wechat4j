package com.wekri;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.oauth.bean.AuthInfo;
import com.wekri.wechat4j.api.response.NewsResponse;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by liuweiguo.
 */
public class Tests {
    static Set<Field> set = new HashSet<>();
    public static void main(String[] args) {
        String s = "{ \"access_token\":\"ACCESS_TOKEN\",\n" +
                "\"expires_in\":7200,\n" +
                "\"refresh_token\":\"REFRESH_TOKEN\",\n" +
                "\"openid\":\"OPENID\",\n" +
                "\"scope\":\"SCOPE\" }";

        AuthInfo res = JSONObject.parseObject(s, AuthInfo.class);
        System.out.println(res.getAccess_token());

        System.out.println(iteratorField(new ArrayList<>(Arrays.asList(NewsResponse.class.getDeclaredFields()))));
    }

    public static List<Field> iteratorField(List<Field> fieldList) {
        if (null != fieldList && !fieldList.isEmpty()) {
            Iterator<Field> iterator = fieldList.iterator();
            while (iterator.hasNext()) {
                Field field = iterator.next();
                set.addAll(iteratorField(new ArrayList<>(Arrays.asList(field.getType().getDeclaredFields()))));
            }
        }
        return fieldList;
    }
}
