package com.wekri.wechat4j.lang;

/**
 * @author liuweiguo.
 */
public enum Language {
    /**
     * 简体中文
     */
    zh_CN,
    /**
     * 繁体中文TW
     */
    zh_TW,
    /**
     * 繁体中文HK
     */
    zh_HK,
    /**
     * 英文
     */
    en,
    /**
     * 印尼
     */
    id,
    /**
     * 马来
     */
    ms,
    /**
     * 西班牙
     */
    es,
    /**
     * 韩国
     */
    ko,
    /**
     * 意大利
     */
    it,
    /**
     * 日本
     */
    ja,
    /**
     * 波兰
     */
    pl,
    /**
     * 葡萄牙
     */
    pt,
    /**
     * 俄国
     */
    ru,
    /**
     * 泰文
     */
    th,
    /**
     * 越南
     */
    vi,
    /**
     * 阿拉伯语
     */
    ar,
    /**
     * 北印度
     */
    hi,
    /**
     * 希伯来
     */
    he,
    /**
     * 土耳其
     */
    tr,
    /**
     * 德语
     */
    de,
    /**
     * 法语
     */
    fr,;
}
