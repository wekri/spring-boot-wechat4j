package com.wekri.wechat4j.spring.autoconfigure;

import com.wekri.wechat4j.api.oauth.OAuthApi;
import org.springframework.context.annotation.Bean;

/**
 * @author liuweiguo.
 */
public class OAuthAutoConfiguration {

    @Bean
    public OAuthApi getOAuthApi() {
        return new OAuthApi();
    }
}
