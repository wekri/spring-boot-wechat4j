package com.wekri.wechat4j.spring.autoconfigure;

import com.wekri.wechat4j.api.marketing.RedPackApi;
import com.wekri.wechat4j.api.material.MaterialApi;
import com.wekri.wechat4j.api.media.MediaApi;
import com.wekri.wechat4j.api.menu.MenuApi;
import com.wekri.wechat4j.api.menu.PersonalizedMenuApi;
import com.wekri.wechat4j.api.message.CustomerMsgApi;
import com.wekri.wechat4j.api.message.TemplateMsgApi;
import com.wekri.wechat4j.api.oauth.OAuthApi;
import com.wekri.wechat4j.api.promotion.QrCodeApi;
import com.wekri.wechat4j.api.user.UserApi;
import com.wekri.wechat4j.api.user.UserTagApi;
import com.wekri.wechat4j.miniprogram.login.LoginApi;
import com.wekri.wechat4j.token.TokenManager;
import com.wekri.wechat4j.token.WeChatTokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author liuweiguo
 * @date 2019/7/26.
 */
@Configuration
@ConditionalOnProperty(name = "wechat4j.enabled", matchIfMissing = true)
public class WeChat4JAutoConfigure {

    @Autowired
    RedisTemplate<String, String> redisTemplate;

    @Bean
    public OAuthApi getOAuthApi() {
        return new OAuthApi();
    }

    @Bean
    public MenuApi getMenuApi() {
        return new MenuApi();
    }

    @Bean
    public PersonalizedMenuApi getPersonalizedMenuApi() {
        return new PersonalizedMenuApi();
    }

    @Bean
    public UserApi getUserApi() {
        return new UserApi();
    }

    @Bean
    public UserTagApi getUserTagApi() {
        return new UserTagApi();
    }

    @Bean
    public QrCodeApi getQrCodeApi() {
        return new QrCodeApi();
    }

    @Bean
    public RedPackApi getRedPackApi() {
        return new RedPackApi();
    }

    @Bean
    public MediaApi getMediaApi() {
        return new MediaApi();
    }

    @Bean
    public TemplateMsgApi getTemplateMsgApi() {
        return new TemplateMsgApi();
    }

    @Bean
    public CustomerMsgApi getCustomerMsgApi() {
        return new CustomerMsgApi();
    }

    @Bean
    public MaterialApi getMaterialApi() {
        return new MaterialApi();
    }

    @Bean
    public TokenManager getTokenManager() {
        return new TokenManager();
    }

    @Bean
    public WeChatTokenManager getWeChatTokenManager() {
        return new WeChatTokenManager(redisTemplate);
    }
    @Bean
    public LoginApi getLoginApi() {
        return new LoginApi();
    }
}
