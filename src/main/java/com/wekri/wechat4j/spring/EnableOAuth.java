package com.wekri.wechat4j.spring;

import com.wekri.wechat4j.spring.autoconfigure.OAuthAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author liuweiguo.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({OAuthAutoConfiguration.class})
public @interface EnableOAuth {
}
