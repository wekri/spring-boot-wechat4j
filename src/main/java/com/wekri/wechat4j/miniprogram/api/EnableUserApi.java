package com.wekri.wechat4j.miniprogram.api;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author liuweiguo.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({UserApiAutoConfiguration.class, UserApiBean.class})
public @interface EnableUserApi {
    String appId() default "";

    String secret() default "";
}
