package com.wekri.wechat4j.miniprogram.api;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

/**
 * Created by liuweiguo.
 */
public class UserApiAutoConfiguration implements ImportSelector {

    static String appId;
    static String secret;

    public String[] selectImports(AnnotationMetadata metadata) {

        AnnotationAttributes attributes = AnnotationAttributes.fromMap(
                metadata.getAnnotationAttributes(EnableUserApi .class.getName(), true));

        this.appId = attributes.getString("appId");
        this.secret = attributes.getString("secret");

        String[] imports = {};
        return imports;
    }
}