package com.wekri.wechat4j.miniprogram.api;

import org.springframework.context.annotation.Bean;

/**
 * Created by liuweiguo.
 */
public class UserApiBean {
    @Bean
    public UserApi getUserApi() {
        return new UserApi(UserApiAutoConfiguration.appId, UserApiAutoConfiguration.secret);
    }
}
