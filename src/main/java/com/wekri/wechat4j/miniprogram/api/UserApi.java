package com.wekri.wechat4j.miniprogram.api;

import com.wekri.wechat4j.miniprogram.bean.AuthInfo;
import com.wekri.wechat4j.util.HttpUtils;

/**
 * Created by liuweiguo
 */
public class UserApi {

    private String url = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code";
    private String appId;
    private String secret;

    public UserApi(String appId, String secret) {
        this.appId = appId;
        this.secret = secret;
    }

    public UserApi setAppId(String appId) {
        this.appId = appId;
        return this;
    }

    public UserApi setSecret(String secret) {
        this.secret = secret;
        return this;
    }

    public AuthInfo loign(String jsCode) {
        return HttpUtils.get(url.replace("APPID", appId).replace("SECRET", secret).replace("JSCODE", jsCode), AuthInfo.class);
    }
}
