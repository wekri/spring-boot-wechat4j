package com.wekri.wechat4j.miniprogram.login;

import com.wekri.wechat4j.util.HttpUtils;

/**
 * @author liuweiguo
 * @date 2020/2/18.
 */
public class LoginApi {

    private static final String login_URI = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code";

    public LoginResult login(String code, String appid, String secret) {
        return HttpUtils.get(login_URI.replace("APPID", appid).replace("SECRET", secret).replace("JSCODE", code), LoginResult.class);
    }
}
