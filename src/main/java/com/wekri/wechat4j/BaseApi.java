package com.wekri.wechat4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author liuweiguo.
 */
public abstract class BaseApi {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
}
