package com.wekri.wechat4j.exception;

/**
 * @author liuweiguo
 * @date 2018/11/6.
 */
public class WeChatException extends RuntimeException {

    public WeChatException() {
        super();
    }

    public WeChatException(String message) {
        super(message);
    }

    public WeChatException(String message, Throwable cause) {
        super(message, cause);
    }

    public WeChatException(Throwable cause) {
        super(cause);
    }
}
