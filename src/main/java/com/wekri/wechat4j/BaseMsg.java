package com.wekri.wechat4j;

import java.io.Serializable;

/**
 * @author liuweiguo.
 */
public class BaseMsg implements Serializable {

    private String successCode = "0";
    private String errcode;
    private String errmsg;

    public String getErrcode() {
        return errcode;
    }

    public void setErrcode(String errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public boolean isSuccess() {
        if (null != errcode) {
            return successCode.equals(errcode);
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "BaseMsg{" +
                "errcode='" + errcode + '\'' +
                ", errmsg='" + errmsg + '\'' +
                '}';
    }
}
