package com.wekri.wechat4j.token;

/**
 * @author liuweiguo
 */
public interface TokenCacheServer {
    String getToken();

    void setToken(String token);

    String getJsapiTicket();

    void setJsapiTicket(String ticket);
}
