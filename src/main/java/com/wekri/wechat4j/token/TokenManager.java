package com.wekri.wechat4j.token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author liuweiguo
 * @date 2019/4/26.
 */
public class TokenManager {

    Logger logger = LoggerFactory.getLogger(TokenManager.class);

    public void updateToken(String weChatAppId, String weChatAppSecret, TokenCacheServer tokenServer) {
        logger.info("更新微信公众号:[{}]token", weChatAppId);

        AccessToken accessToken = new AccessToken(weChatAppId, weChatAppSecret);
        if (accessToken.request()) {
            tokenServer.setToken(accessToken.getAccess_token());
            logger.info("微信公众号:[{}]获取token成功，token:{}",
                    weChatAppId, accessToken.getAccess_token());

            Ticket ticket = new Ticket(accessToken.getAccess_token());
            if (ticket.request()) {
                tokenServer.setJsapiTicket(ticket.getTicket());
                logger.info("微信公众号:[{}]获取JsapiTicket成功，JsapiTicket:{}",
                        weChatAppId, ticket.getTicket());
            } else {
                logger.error("微信公众号:[{}]获取JsapiTicket失败", weChatAppId);
            }
        } else {
            logger.error("微信公众号:[{}]获取token失败", weChatAppId);
        }
    }
}
