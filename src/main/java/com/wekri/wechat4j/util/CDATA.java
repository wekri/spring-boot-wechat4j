package com.wekri.wechat4j.util;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author liuweiguo
 * @date 2018/11/2.
 */
@Retention(RUNTIME)
@Target({FIELD, METHOD})
public @interface CDATA {
    /**
     * xml中节点名
     *
     * @return
     */
    String value() default "";
}
