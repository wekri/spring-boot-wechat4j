package com.wekri.wechat4j.util;

import java.io.InputStream;

/**
 * @author liuweiguo
 * @date 2019/7/16.
 */
public class Cert {

    /**
     * 证书输入流
     */
    InputStream certInputStream;
    /**
     * 密码，密码为商户 Mch ID
     */
    String password;

    public Cert(InputStream certInputStream, String password) {
        this.certInputStream = certInputStream;
        this.password = password;
    }

    public InputStream getCertInputStream() {
        return certInputStream;
    }

    public void setCertInputStream(InputStream certInputStream) {
        this.certInputStream = certInputStream;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
