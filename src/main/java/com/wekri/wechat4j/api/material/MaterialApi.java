package com.wekri.wechat4j.api.material;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.material.bean.*;
import com.wekri.wechat4j.util.HttpUtils;

import java.io.File;

/**
 * 素材管理
 *
 * @author liuweiguo
 */
public class MaterialApi {
    /**
     * 获取永久素材
     * http请求方式: POST,https协议
     */
    private String POST_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=";
    /**
     * 删除永久素材
     * http请求方式: POST
     */
    private String POST_DELETE_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=";

    /**
     * 获取素材总数
     * http请求方式: GET
     */
    private String GET_MATERIAL_COUNT = "https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=";

    /**
     * 获取永久素材的列表
     * http请求方式: POST
     */
    private String POST_MATERIAL_LIST = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=";

    /**
     * 新增永久图文素材
     */
    private String POST_ADD_NEWS = "https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=";

    /**
     * 新增其他类型永久素材
     * 通过POST表单来调用接口，表单id为media
     */
    private String POST_ADD_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=";

    /**
     * 获取素材总数
     *
     * @param accessToken
     * @return
     */
    public MaterialCountResult materialCount(String accessToken) {
        return HttpUtils.get(GET_MATERIAL_COUNT + accessToken, MaterialCountResult.class);
    }

    /**
     * 获取永久素材的列表，临时素材无法通过本接口获取
     *
     * @param accessToken
     * @param type        素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
     * @param offset      从全部素材的该偏移位置开始返回，0表示从第一个素材 返回
     * @param count       返回素材的数量，取值在1到20之间
     * @return
     */
    public MaterialListResult materialList(String accessToken, MaterialType type, Integer offset, Integer count) {
        JSONObject param = new JSONObject();
        param.put("type", type);
        param.put("offset", offset);
        param.put("count", count);
        return HttpUtils.postJson(POST_MATERIAL_LIST + accessToken, param.toJSONString(), MaterialListResult.class);
    }

    /**
     * 获取永久素材
     *
     * @param accessToken 调用接口凭证
     * @param mediaId     要获取的素材的media_id
     * @return
     */
    public MaterialResult getMaterial(String accessToken, String mediaId) {
        JSONObject param = new JSONObject();
        param.put("media_id", mediaId);
        return HttpUtils.getMaterial(POST_MATERIAL + accessToken, JSONObject.toJSONString(param));
    }

    /**
     * 新增其他类型永久素材
     * <p>
     * 接口调用请求说明
     * <p>
     * 通过POST表单来调用接口，表单id为media，包含需要上传的素材内容，有filename、filelength、content-type等信息。请注意：图片素材将进入公众平台官网素材管理模块中的默认分组。
     * <p>
     *
     * @param accessToken
     * @param type        媒体文件类型，分别有
     *                    <ul>
     *                    <li>图片（image）: 2M，支持bmp/png/jpeg/jpg/gif格式</li>
     *                    <li>语音（voice）：2M，播放长度不超过60s，mp3/wma/wav/amr格式</li>
     *                    <li>视频（video）：10MB，支持MP4格式</li>
     *                    <li>缩略图（thumb）：64KB，支持JPG格式</li>
     *                    </ul>
     * @param file        媒体文件
     * @return
     */
    public AddMaterialResult addMaterial(String accessToken, MaterialType type, File file) {
        return HttpUtils.upload(new StringBuilder(POST_ADD_MATERIAL).append(accessToken)
                .append("&type=").append(type).toString(), file, AddMaterialResult.class);
    }

    /**
     * 删除永久素材
     *
     * @param accessToken 调用接口凭证
     * @param mediaId     要获取的素材的media_id
     * @return
     */
    public BaseMsg deleteMaterial(String accessToken, String mediaId) {
        JSONObject param = new JSONObject();
        param.put("media_id", mediaId);
        return HttpUtils.postJson(POST_DELETE_MATERIAL + accessToken, JSONObject.toJSONString(param), BaseMsg.class);
    }
}
