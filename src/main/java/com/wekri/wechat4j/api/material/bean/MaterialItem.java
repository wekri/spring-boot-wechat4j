package com.wekri.wechat4j.api.material.bean;

/**
 * @author liuweiguo
 */
public class MaterialItem {
    private String media_id;
    private Long update_time;
    private String name;
    private String url;
    private MaterialContent content;

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

    public Long getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Long update_time) {
        this.update_time = update_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public MaterialContent getContent() {
        return content;
    }

    public void setContent(MaterialContent content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "MaterialItem{" +
                "media_id='" + media_id + '\'' +
                ", update_time=" + update_time +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", content=" + content +
                '}';
    }
}
