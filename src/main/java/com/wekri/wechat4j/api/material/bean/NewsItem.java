package com.wekri.wechat4j.api.material.bean;

/**
 * @author liuweiguo
 */
public class NewsItem {
    private String title;//"孩儿乐返现活动",
    private String author;//"刘伟国",
    private String digest;//"一、邀请由管理人员在“童慧商贸育儿课堂”回复“添加店员”，让店员扫码关注即邀请成功。二、绑定店员在公众号依次",
    private String content;//"<p><span style="font-size: 20px;">一、邀请</span></p><section style="text-indent: 2em;">由管理人员在“童慧商贸育儿课堂”回复“添加店员”，让店员扫码关注即邀请成功。<br /></section><p><span style="font-size: 20px;">二、绑定</span></p><section style="text-indent: 2em;">店员在公众号依次回复“添加姓名”，“添加店名”，“添加地址”，根据提示绑定相关信息即可。</section><section style="text-indent: 0em;"><span style="font-size: 20px;text-align: left;text-indent: 2em;">三</span><span style="font-size: 20px;text-align: left;text-indent: 2em;">、领红包</span></section><section style="text-indent: 2em;"><span style="text-indent: 2em;">店员售出产品时，刮开产品二维码，在“童慧商贸育儿课堂”下面菜单点击“扫码”菜单，扫描防伪码，即可领取红包。说明：前两个红包会即时发放，后续红包每隔3个小时汇总发放到公众号，请注意查收。</span></section>",
    private String content_source_url;//"",
    private String thumb_media_id;//"cqWuDl6VCjEd02Yx1crogX0gb6kYWU2id5J495AQAyc",
    private Integer show_cover_pic;//0,
    private String url;//"http://mp.weixin.qq.com/s?__biz=MzU2Mjg4NTQ5Mg==&mid=100000002&idx=1&sn=678620f7717afa0452b1ec758f4abba3&chksm=7c63e7194b146e0f947c2f500a2023b9c113880e7d1adae22c7b28510cd106751452e214cf3f#rd",
    private String thumb_url;//"http://mmbiz.qpic.cn/mmbiz_jpg/wibpzQJuoNSy8o4emYzv8bmZwR5wEG1fqOicPfmSmBRJYXHIt6iakAFibkxC0iaka3c1QG9vdkZffiagU9KSlh7Nf5ag/0?wx_fmt=jpeg",
    private Integer need_open_comment;//0,
    private Integer only_fans_can_comment;//0

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent_source_url() {
        return content_source_url;
    }

    public void setContent_source_url(String content_source_url) {
        this.content_source_url = content_source_url;
    }

    public String getThumb_media_id() {
        return thumb_media_id;
    }

    public void setThumb_media_id(String thumb_media_id) {
        this.thumb_media_id = thumb_media_id;
    }

    public Integer getShow_cover_pic() {
        return show_cover_pic;
    }

    public void setShow_cover_pic(Integer show_cover_pic) {
        this.show_cover_pic = show_cover_pic;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumb_url() {
        return thumb_url;
    }

    public void setThumb_url(String thumb_url) {
        this.thumb_url = thumb_url;
    }

    public Integer getNeed_open_comment() {
        return need_open_comment;
    }

    public void setNeed_open_comment(Integer need_open_comment) {
        this.need_open_comment = need_open_comment;
    }

    public Integer getOnly_fans_can_comment() {
        return only_fans_can_comment;
    }

    public void setOnly_fans_can_comment(Integer only_fans_can_comment) {
        this.only_fans_can_comment = only_fans_can_comment;
    }

    @Override
    public String toString() {
        return "NewsItem{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", digest='" + digest + '\'' +
                ", content='" + content + '\'' +
                ", content_source_url='" + content_source_url + '\'' +
                ", thumb_media_id='" + thumb_media_id + '\'' +
                ", show_cover_pic=" + show_cover_pic +
                ", url='" + url + '\'' +
                ", thumb_url='" + thumb_url + '\'' +
                ", need_open_comment=" + need_open_comment +
                ", only_fans_can_comment=" + only_fans_can_comment +
                '}';
    }
}
