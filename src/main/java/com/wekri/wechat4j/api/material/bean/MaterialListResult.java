package com.wekri.wechat4j.api.material.bean;

import com.wekri.wechat4j.BaseMsg;

import java.util.List;

/**
 * @author liuweiguo
 */
public class MaterialListResult extends BaseMsg {
    private Integer total_count;
    private Integer item_count;
    private List<MaterialItem> item;

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public Integer getItem_count() {
        return item_count;
    }

    public void setItem_count(Integer item_count) {
        this.item_count = item_count;
    }

    public List<MaterialItem> getItem() {
        return item;
    }

    public void setItem(List<MaterialItem> item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "MaterialListResult{" +
                "total_count=" + total_count +
                ", item_count=" + item_count +
                ", item=" + item +
                "} " + super.toString();
    }
}
