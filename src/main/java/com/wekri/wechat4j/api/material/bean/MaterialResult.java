package com.wekri.wechat4j.api.material.bean;

import com.wekri.wechat4j.BaseMsg;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * 获取永久素材结果
 *
 * @author liuweiguo
 */
public class MaterialResult extends BaseMsg {
    /**
     * 接口返回类型
     */
    private String responseType;

    /**
     * 视频消息-标题
     */
    private String title;
    /**
     * 视频消息-描述
     */
    private String description;
    /**
     * 视频消息-下载地址
     */
    private String down_url;

    /**
     * 图文素材
     */
    private List news_item;

    /**
     * 图片类型的素材，则响应的直接为素材的内容，开发者可以自行保存为文件。
     */
    private byte[] bytes;

    /**
     * 下载到指定目录
     *
     * @param path
     * @throws Exception
     */
    public void download(String path) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
        IOUtils.write(this.bytes, fileOutputStream);
        IOUtils.closeQuietly(fileOutputStream);
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDown_url() {
        return down_url;
    }

    public void setDown_url(String down_url) {
        this.down_url = down_url;
    }

    public List getNews_item() {
        return news_item;
    }

    public void setNews_item(List news_item) {
        this.news_item = news_item;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
