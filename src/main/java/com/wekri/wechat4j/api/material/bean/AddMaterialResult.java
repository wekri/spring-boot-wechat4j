package com.wekri.wechat4j.api.material.bean;

import com.wekri.wechat4j.BaseMsg;

/**
 * @author liuweiguo
 */
public class AddMaterialResult extends BaseMsg {
    private String media_id;
    private String url;

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "AddMaterialResult{" +
                "media_id='" + media_id + '\'' +
                ", url='" + url + '\'' +
                "} " + super.toString();
    }
}
