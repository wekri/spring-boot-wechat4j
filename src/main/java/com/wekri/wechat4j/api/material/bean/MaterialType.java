package com.wekri.wechat4j.api.material.bean;

/**
 * 素材类型
 *
 * @author liuweiguo
 */
public enum MaterialType {
    /**
     * 图片
     */
    image,
    /**
     * 视频
     */
    video,
    /**
     * 语音
     */
    voice,
    /**
     * 图文
     */
    news,
    /**
     * 缩略图
     */
    thumb;
}
