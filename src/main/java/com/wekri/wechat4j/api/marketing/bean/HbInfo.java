package com.wekri.wechat4j.api.marketing.bean;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 查询红包记录返回结果
 *
 * @author liuweiguo
 * @date 2019/7/17.
 */
@XmlRootElement(name = "hbinfo")
public class HbInfo {
    /**
     * 领取红包的openid
     */
    private String openid;
    /**
     * 领取金额（分）
     */
    private Integer amount;
    /**
     * 接收时间
     */
    private String rcv_time;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getRcv_time() {
        return rcv_time;
    }

    public void setRcv_time(String rcv_time) {
        this.rcv_time = rcv_time;
    }

    @Override
    public String toString() {
        return "HbInfo{" +
                "openid='" + openid + '\'' +
                ", amount=" + amount +
                ", rcv_time='" + rcv_time + '\'' +
                '}';
    }
}
