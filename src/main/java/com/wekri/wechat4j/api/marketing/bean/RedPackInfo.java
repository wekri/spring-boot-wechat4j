package com.wekri.wechat4j.api.marketing.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 查询红包记录返回结果
 *
 * @author wekri
 * @date 2019/7/17.
 */
@XmlRootElement(name = "xml")
public class RedPackInfo {

    /**
     * 返回状态码
     * SUCCESS/FAIL
     * 此字段是通信标识，非红包发放结果标识，红包发放是否成功需要结合result_code以及status来判断
     */
    private String return_code;
    /**
     * 返回信息，
     * 如非空，为错误原因
     * 签名失败
     * 参数格式校验错误
     */
    private String return_msg;



    /**
     * 业务结果
     * SUCCESS/FAIL
     * 非红包发放结果标识，红包发放是否成功需要查看status字段来判断
     */
    private String result_code;
    /**
     * 错误码信息
     */
    private String err_code;
    /**
     * 结果信息描述
     */
    private String err_code_des;



    /**
     * 我方订单号，商户使用查询API填写的商户单号的原路返回
     */
    private String mch_billno;
    /**
     * 微信支付分配的商户号
     */
    private String mch_id;
    /**
     * 微信单号，使用API发放现金红包时返回的红包单号
     */
    private String detail_id;
    /**
     * SENDING:发放中
     * SENT:已发放待领取
     * FAILED：发放失败
     * RECEIVED:已领取
     * RFUND_ING:退款中
     * REFUND:已退款
     */
    private String status;
    /**
     * API:通过API接口发放
     * UPLOAD:通过上传文件方式发放
     * ACTIVITY:通过活动方式发放
     */
    private String send_type;
    /**
     * GROUP:裂变红包
     * NORMAL:普通红包
     */
    private String hb_type;
    /**
     * 红包个数
     */
    private String total_num;
    /**
     * 红包总金额（单位分）
     */
    private Integer total_amount;
    /**
     * 发送失败原因
     */
    private String reason;
    /**
     * 红包发送时间
     */
    private String send_time;
    /**
     * 红包退款金额
     */
    private String refund_time;
    /**
     * 红包退款金额
     */
    private Integer refund_amount;
    /**
     * 祝福语
     */
    private String wishing;
    /**
     * 活动描述，低版本微信可见
     */
    private String remark;
    /**
     * 发红包的活动名称
     */
    private String act_name;
    /**
     * 红包的领取列表
     */
    private HbList hbList;

    public boolean isSuccess(){
        return "SUCCESS".equals(return_code) && "SUCCESS".equals(result_code);
    }

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_msg() {
        return return_msg;
    }

    public void setReturn_msg(String return_msg) {
        this.return_msg = return_msg;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getErr_code() {
        return err_code;
    }

    public void setErr_code(String err_code) {
        this.err_code = err_code;
    }

    public String getErr_code_des() {
        return err_code_des;
    }

    public void setErr_code_des(String err_code_des) {
        this.err_code_des = err_code_des;
    }

    public String getMch_billno() {
        return mch_billno;
    }

    public void setMch_billno(String mch_billno) {
        this.mch_billno = mch_billno;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getDetail_id() {
        return detail_id;
    }

    public void setDetail_id(String detail_id) {
        this.detail_id = detail_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSend_type() {
        return send_type;
    }

    public void setSend_type(String send_type) {
        this.send_type = send_type;
    }

    public String getHb_type() {
        return hb_type;
    }

    public void setHb_type(String hb_type) {
        this.hb_type = hb_type;
    }

    public String getTotal_num() {
        return total_num;
    }

    public void setTotal_num(String total_num) {
        this.total_num = total_num;
    }

    public Integer getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Integer total_amount) {
        this.total_amount = total_amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getSend_time() {
        return send_time;
    }

    public void setSend_time(String send_time) {
        this.send_time = send_time;
    }

    public String getRefund_time() {
        return refund_time;
    }

    public void setRefund_time(String refund_time) {
        this.refund_time = refund_time;
    }

    public Integer getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(Integer refund_amount) {
        this.refund_amount = refund_amount;
    }

    public String getWishing() {
        return wishing;
    }

    public void setWishing(String wishing) {
        this.wishing = wishing;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAct_name() {
        return act_name;
    }

    public void setAct_name(String act_name) {
        this.act_name = act_name;
    }

    @XmlElement(name = "hblist")
    public HbList getHbList() {
        return hbList;
    }

    public void setHbList(HbList hbList) {
        this.hbList = hbList;
    }

    @Override
    public String toString() {
        return "RedPackInfo{" +
                "return_code='" + return_code + '\'' +
                ", return_msg='" + return_msg + '\'' +
                ", result_code='" + result_code + '\'' +
                ", err_code='" + err_code + '\'' +
                ", err_code_des='" + err_code_des + '\'' +
                ", mch_billno='" + mch_billno + '\'' +
                ", mch_id='" + mch_id + '\'' +
                ", detail_id='" + detail_id + '\'' +
                ", status='" + status + '\'' +
                ", send_type='" + send_type + '\'' +
                ", hb_type='" + hb_type + '\'' +
                ", total_num='" + total_num + '\'' +
                ", total_amount=" + total_amount +
                ", reason='" + reason + '\'' +
                ", send_time='" + send_time + '\'' +
                ", refund_time='" + refund_time + '\'' +
                ", refund_amount=" + refund_amount +
                ", wishing='" + wishing + '\'' +
                ", remark='" + remark + '\'' +
                ", act_name='" + act_name + '\'' +
                ", hbList=" + hbList +
                '}';
    }
}