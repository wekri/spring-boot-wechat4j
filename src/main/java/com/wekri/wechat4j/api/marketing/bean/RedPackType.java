package com.wekri.wechat4j.api.marketing.bean;

/**
 * 红包类型
 *
 * @author liuweiguo
 * @date 2019/7/17.
 */
public enum RedPackType {

    /**
     * 裂变红包
     */
    GROUP,
    /**
     * 普通红包
     */
    NORMAL,;
}
