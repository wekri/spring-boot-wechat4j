package com.wekri.wechat4j.api.marketing.bean;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <xml>
 * <return_code><![CDATA[SUCCESS]]></return_code>
 * <return_msg><![CDATA[IP地址非你在商户平台设置的可用IP地址]]></return_msg>
 * <result_code><![CDATA[FAIL]]></result_code>
 * <err_code><![CDATA[NO_AUTH]]></err_code>
 * <err_code_des><![CDATA[IP地址非你在商户平台设置的可用IP地址]]></err_code_des>
 * <mch_billno><![CDATA[2019071618032447677886491718]]></mch_billno>
 * <mch_id><![CDATA[1532238391]]></mch_id>
 * <wxappid><![CDATA[wxa84379b511a3448e]]></wxappid>
 * <re_openid><![CDATA[odFFd1GPtqc-23PSWQ49ofvzO_do]]></re_openid>
 * <total_amount>100</total_amount>
 * </xml>
 *
 * @author liuweiguo
 * @date 2019/7/17.
 */
@XmlRootElement(name = "xml")
public class RedPackResult {
    /**
     * SUCCESS/FAIL
     * 此字段是通信标识，非红包发放结果标识，红包发放是否成功需要查看result_code来判断
     */
    private String return_code;
    /**
     * 返回信息，如非空，为错误原因
     * 签名失败
     * 参数格式校验错误
     * 发放成功
     */
    private String return_msg;



    /**
     * UCCESS/FAIL
     * 注意：当状态为FAIL时，存在业务结果未明确的情况。
     * 所以如果状态是FAIL，请务必再请求一次查询接口[请务必关注错误代码（err_code字段），通过查询得到的红包状态确认此次发放的结果。]，
     * 以确认此次发放的结果。
     */
    private String result_code;
    /**
     * 错误码信息
     * 注意：出现未明确的错误码（SYSTEMERROR等）时，
     * 请务必用原商户订单号重试，或者再请求一次查询接口以确认此次发放的结果。
     */
    private String err_code;
    /**
     * 发放成功/IP地址非你在商户平台设置的可用IP地址
     * 结果信息描述
     */
    private String err_code_des;



    /**
     * 商户订单号（每个订单号必须唯一）
     * 组成：mch_id+yyyymmdd+10位一天内不能重复的数字
     * String(28)
     */
    private String mch_billno;
    /**
     * 微信支付分配的商户号
     */
    private String mch_id;
    /**
     * 公众号appid
     */
    private String wxappid;
    /**
     * 接收红包用户在公众号下的openid
     */
    private String re_openid;
    /**
     * 红包金额（分）
     */
    private String total_amount;
    /**
     * 红包订单的微信单号
     */
    private String send_listid;

    public boolean isSuccess() {
        return "SUCCESS".equals(return_code) && "SUCCESS".equals(result_code);
    }

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_msg() {
        return return_msg;
    }

    public void setReturn_msg(String return_msg) {
        this.return_msg = return_msg;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getErr_code() {
        return err_code;
    }

    public void setErr_code(String err_code) {
        this.err_code = err_code;
    }

    public String getErr_code_des() {
        return err_code_des;
    }

    public void setErr_code_des(String err_code_des) {
        this.err_code_des = err_code_des;
    }

    public String getMch_billno() {
        return mch_billno;
    }

    public void setMch_billno(String mch_billno) {
        this.mch_billno = mch_billno;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getWxappid() {
        return wxappid;
    }

    public void setWxappid(String wxappid) {
        this.wxappid = wxappid;
    }

    public String getRe_openid() {
        return re_openid;
    }

    public void setRe_openid(String re_openid) {
        this.re_openid = re_openid;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getSend_listid() {
        return send_listid;
    }

    public void setSend_listid(String send_listid) {
        this.send_listid = send_listid;
    }

    @Override
    public String toString() {
        return "RedPackResult{" +
                "return_code='" + return_code + '\'' +
                ", return_msg='" + return_msg + '\'' +
                ", result_code='" + result_code + '\'' +
                ", err_code='" + err_code + '\'' +
                ", err_code_des='" + err_code_des + '\'' +
                ", mch_billno='" + mch_billno + '\'' +
                ", mch_id='" + mch_id + '\'' +
                ", wxappid='" + wxappid + '\'' +
                ", re_openid='" + re_openid + '\'' +
                ", total_amount='" + total_amount + '\'' +
                ", send_listid='" + send_listid + '\'' +
                '}';
    }
}