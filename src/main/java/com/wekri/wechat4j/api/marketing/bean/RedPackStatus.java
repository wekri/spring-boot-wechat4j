package com.wekri.wechat4j.api.marketing.bean;

/**
 * 红包状态
 *
 * @author liuweiguo
 * @date 2019/7/17.
 */
public enum RedPackStatus {
    /**
     * 发放中
     */
    SENDING,
    /**
     * 已发放待领取
     */
    SENT,
    /**
     * 发放失败
     */
    FAILED,
    /**
     * 已领取
     */
    RECEIVED,
    /**
     * 退款中
     */
    RFUND_ING,
    /**
     * 已退款
     */
    REFUND,;
}
