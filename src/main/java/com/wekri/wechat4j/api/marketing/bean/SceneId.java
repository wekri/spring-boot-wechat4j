package com.wekri.wechat4j.api.marketing.bean;

/**
 * 发放红包使用场景，红包金额大于200或者小于1元时必传
 *
 * @author liuweiguo
 * @date 2019/7/29.
 */
public enum SceneId {
    /**
     * 商品促销，额度：1.00 - 499.00元
     */
    PRODUCT_1,
    /**
     * 抽奖，额度：1.00 - 200.00元
     */
    PRODUCT_2,
    /**
     * 虚拟物品兑奖
     */
    PRODUCT_3,
    /**
     * 企业内部福利，额度：1.00 - 499.00元
     */
    PRODUCT_4,
    /**
     * 渠道分润，额度：1.00 - 200.00元
     */
    PRODUCT_5,
    /**
     * 保险回馈
     */
    PRODUCT_6,
    /**
     * 彩票派奖
     */
    PRODUCT_7,
    /**
     * 税务刮奖
     */
    PRODUCT_8,;
}
