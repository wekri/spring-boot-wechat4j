package com.wekri.wechat4j.api.marketing.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * 查询红包记录返回结果
 *
 * @author wekri
 * @date 2019/7/17.
 */
@XmlRootElement(name = "hblist")
public class HbList {

    private List<HbInfo> hbList;

    @XmlElement(name = "hbinfo")

    public List<HbInfo> getHbList() {
        return hbList;
    }

    public void setHbList(List<HbInfo> hbList) {
        this.hbList = hbList;
    }

    @Override
    public String toString() {
        return "HbList{" +
                "hbList=" + hbList +
                '}';
    }
}