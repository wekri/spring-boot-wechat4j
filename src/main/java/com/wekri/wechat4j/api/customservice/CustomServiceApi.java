package com.wekri.wechat4j.api.customservice;

/**
 * 新版客服功能
 *
 * @author liuweiguo
 */
public class CustomServiceApi {

    /**
     * 获取客服基本信息
     * http请求方式: GET
     */
    private static String GET_KF_LIST = "https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=";
    /**
     * 添加客服帐号
     * http请求方式: POST
     */
    private static String POST_ADD_ACCOUNT = "https://api.weixin.qq.com/customservice/kfaccount/add?access_token=";

    /**
     * 邀请绑定客服帐号
     * 新添加的客服帐号是不能直接使用的，只有客服人员用微信号绑定了客服账号后，方可登录Web客服进行操作。
     * 此接口发起一个绑定邀请到客服人员微信号，客服人员需要在微信客户端上用该微信号确认后帐号才可用。
     * 尚未绑定微信号的帐号可以进行绑定邀请操作，邀请未失效时不能对该帐号进行再次绑定微信号邀请。
     * http请求方式: POST
     */
    private static String POST_INVITE_WORKER = "https://api.weixin.qq.com/customservice/kfaccount/inviteworker?access_token=";
    /**
     * 设置客服信息
     * http请求方式: POST
     */
    private static String POST_ACCOUNT_UPDATE = "https://api.weixin.qq.com/customservice/kfaccount/update?access_token=";
    /**
     * 上传客服头像
     * http请求方式: POST/FORM
     */
    private static String POST_ACCOUNT_UPLOADHEADIMG = "https://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=";
    /**
     * 删除客服帐号
     * http请求方式: GET
     */
    private static String GET_ACCOUNT_DEL = "https://api.weixin.qq.com/customservice/kfaccount/del?access_token=";

}
