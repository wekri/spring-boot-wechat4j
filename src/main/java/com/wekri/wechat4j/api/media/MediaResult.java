package com.wekri.wechat4j.api.media;

import com.wekri.wechat4j.BaseMsg;

/**
 * @author liuweiguo
 * @date 2018/11/5.
 */
public class MediaResult extends BaseMsg {

    /**
     * 商户图片url
     */
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
