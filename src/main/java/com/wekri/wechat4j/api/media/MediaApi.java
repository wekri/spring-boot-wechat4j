package com.wekri.wechat4j.api.media;

import com.wekri.wechat4j.api.material.bean.MaterialResult;
import com.wekri.wechat4j.util.HttpUtils;

import java.io.File;

/**
 * 上传图片
 *
 * @author liuweiguo
 * @date 2018/11/5.
 */
public class MediaApi {
    private static final String MEDIA_UPLOAD_POST = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=";

    /**
     * http请求方式: GET,https调用
     */
    private static final String GET_MEDIA_GET = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";

    /**
     * 上传图片
     * 限制文件大小限制1MB，仅支持JPG、PNG格式。
     *
     * @param file
     * @param accessToken
     * @return
     */
    public MediaResult mediaUpload(File file, String accessToken) {
        return HttpUtils.upload(MEDIA_UPLOAD_POST + accessToken, file, MediaResult.class);
    }

    /**
     * 下载媒体文件
     *
     * @param accessToken
     * @param mediaId
     * @return
     */
    public MaterialResult mediaDownload(String accessToken, String mediaId) {
        return HttpUtils.getMaterial(GET_MEDIA_GET.replace("ACCESS_TOKEN", accessToken).replace("MEDIA_ID", mediaId), null);
    }
}
