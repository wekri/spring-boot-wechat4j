package com.wekri.wechat4j.api.user.bean;

import java.util.List;

/**
 * @author liuweiguo
 * @date 2019/4/24.
 */
public class UserTagResult {
    private List<Integer> tagid_list;

    public List<Integer> getTagid_list() {
        return tagid_list;
    }

    public void setTagid_list(List<Integer> tagid_list) {
        this.tagid_list = tagid_list;
    }
}
