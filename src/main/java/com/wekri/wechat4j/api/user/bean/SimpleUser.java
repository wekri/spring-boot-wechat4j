package com.wekri.wechat4j.api.user.bean;

import com.wekri.wechat4j.lang.Language;

/**
 * 批量获取用户信息参数
 * @author liuweiguo
 * @date 2018/11/2.
 */
public class SimpleUser {

    private String openid;
    private Language lang;

    public SimpleUser(String openid) {
        this.openid = openid;
        this.lang = Language.zh_CN;
    }

    public SimpleUser(String openid, Language lang) {
        this.openid = openid;
        this.lang = lang;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Language getLang() {
        return lang;
    }

    public void setLang(Language lang) {
        this.lang = lang;
    }
}
