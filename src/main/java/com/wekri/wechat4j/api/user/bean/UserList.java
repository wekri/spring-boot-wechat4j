package com.wekri.wechat4j.api.user.bean;

import com.wekri.wechat4j.BaseMsg;

/**
 * @author liuweiguo
 * @date 2019/1/16.
 */
public class UserList extends BaseMsg {
    /**
     * 关注该公众账号的总用户数
     */
    private Integer total;
    /**
     * 拉取的OPENID个数，最大值为10000
     */
    private Integer count;
    /**
     * 拉取列表的最后一个用户的OPENID，下次调用传此参数
     */
    private String next_openid;
    /**
     * 列表数据，OPENID的列表
     */
    private UserListData data;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext_openid() {
        return next_openid;
    }

    public void setNext_openid(String next_openid) {
        this.next_openid = next_openid;
    }

    public UserListData getData() {
        return data;
    }

    public void setData(UserListData data) {
        this.data = data;
    }
}
