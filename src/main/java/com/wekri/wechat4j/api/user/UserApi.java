package com.wekri.wechat4j.api.user;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.user.bean.*;
import com.wekri.wechat4j.lang.Language;
import com.wekri.wechat4j.util.HttpUtils;

import java.util.List;

/**
 * 用户管理
 * @author liuweiguo.
 */
public class UserApi {

    /**
     * 获取用户基本信息（包括UnionID机制）
     * http请求方式: GET
     */
    private static String GET_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info";

    /**
     * 批量获取用户基本信息
     * http请求方式: POST
     */
    private static String POST_USER_INFO_BATCH = "https://api.weixin.qq.com/cgi-bin/user/info/batchget";

    /**
     * 获取用户列表
     * http请求方式: GET
     */
    private static String GET_USER_LIST = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=";

    /**
     * 公众号迁移
     */
    private static String POST_CHANGE_OPENID = "http://api.weixin.qq.com/cgi-bin/changeopenid?access_token=";

    /**
     * 获取用户基本信息（包括UnionID机制）
     *
     * @param accessToken 调用接口凭证
     * @param openId      普通用户的标识，对当前公众号唯一
     * @return
     */
    public UserInfo getUserInfo(String accessToken, String openId) {
        return getUserInfo(accessToken, openId, Language.zh_CN);
    }

    /**
     * 获取用户基本信息（包括UnionID机制）
     * <a href=https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140839>doc<a/>
     *
     * @param accessToken 调用接口凭证
     * @param openId      普通用户的标识，对当前公众号唯一
     * @param lang        返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     */
    public UserInfo getUserInfo(String accessToken, String openId, Language lang) {
        StringBuilder url = new StringBuilder(GET_USER_INFO)
                .append("?access_token=").append(accessToken)
                .append("&openid=").append(openId)
                .append("&lang=").append(lang);
        return HttpUtils.get(url.toString(), UserInfo.class);
    }

    public UserInfoList getUserInfoBatch(String accessToken, List<SimpleUser> userList) {
        StringBuilder url = new StringBuilder(POST_USER_INFO_BATCH);
        url.append("?access_token=").append(accessToken);

        JSONObject json = new JSONObject();
        json.put("user_list", userList);
        return HttpUtils.postJson(url.toString(), json.toJSONString(), UserInfoList.class);
    }

    /**
     * 获取用户列表
     *
     * @param accessToken 调用接口凭证
     * @param nextOpenId  第一个拉取的OPENID，不填默认从头开始拉取
     * @return
     */
    public UserList getUserList(String accessToken, String nextOpenId) {
        StringBuilder url = new StringBuilder(GET_USER_LIST);
        url.append(accessToken).append("&next_openid=").append(nextOpenId);
        return HttpUtils.get(url.toString(), UserList.class);
    }

    /**
     * 微信公众号迁移，获取新公众号openId
     *
     * @param accessToken 新帐号的token
     * @param fromAppId   原帐号的appid
     * @param openIdList  原帐号的openId
     * @return
     */
    public ChangeResult changeOpenId(String accessToken, String fromAppId, List<String> openIdList) {
        JSONObject json = new JSONObject();
        json.put("from_appid", fromAppId);
        json.put("openid_list", openIdList);
        return HttpUtils.postJson(POST_CHANGE_OPENID + accessToken, json.toJSONString(), ChangeResult.class);
    }
}
