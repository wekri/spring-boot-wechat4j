package com.wekri.wechat4j.api.user.bean;

/**
 * @author liuweiguo
 * @date 2019/4/24.
 */
public class TagUserResult {
    private Integer count;
    private String next_openid;
    private TagData data;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext_openid() {
        return next_openid;
    }

    public void setNext_openid(String next_openid) {
        this.next_openid = next_openid;
    }

    public TagData getData() {
        return data;
    }

    public void setData(TagData data) {
        this.data = data;
    }
}
