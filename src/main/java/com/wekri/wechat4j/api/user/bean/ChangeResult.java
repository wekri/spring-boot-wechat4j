package com.wekri.wechat4j.api.user.bean;

import com.wekri.wechat4j.BaseMsg;

import java.util.List;

/**
 * @author liuweiguo
 * @date 2019/1/16.
 */
public class ChangeResult extends BaseMsg {
    private List<Change> result_list;

    public List<Change> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<Change> result_list) {
        this.result_list = result_list;
    }
}