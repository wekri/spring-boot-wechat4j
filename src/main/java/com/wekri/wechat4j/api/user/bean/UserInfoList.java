package com.wekri.wechat4j.api.user.bean;

import java.util.List;

/**
 * 批量获取用户基本信息返回结果
 * @author liuweiguo
 * @date 2018/11/2.
 */
public class UserInfoList {
    private List<UserInfo> user_info_list;

    public List<UserInfo> getUser_info_list() {
        return user_info_list;
    }

    public void setUser_info_list(List<UserInfo> user_info_list) {
        this.user_info_list = user_info_list;
    }

    @Override
    public String toString() {
        return "UserInfoList{" +
                "user_info_list=" + user_info_list +
                '}';
    }
}
