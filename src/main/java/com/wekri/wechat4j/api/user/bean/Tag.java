package com.wekri.wechat4j.api.user.bean;

import com.wekri.wechat4j.BaseMsg;

/**
 * @author liuweiguo
 * @date 2019/4/23.
 */
public class Tag extends BaseMsg {
    private Integer id;
    private String name;
    private Integer count;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
