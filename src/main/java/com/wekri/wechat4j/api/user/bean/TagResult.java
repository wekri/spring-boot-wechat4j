package com.wekri.wechat4j.api.user.bean;

import com.wekri.wechat4j.BaseMsg;

/**
 * @author liuweiguo
 * @date 2019/4/23.
 */
public class TagResult extends BaseMsg{
    private Tag tag;

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
