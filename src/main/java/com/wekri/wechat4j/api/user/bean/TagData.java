package com.wekri.wechat4j.api.user.bean;

import java.util.List;

/**
 * @author liuweiguo
 * @date 2019/4/24.
 */
public class TagData {
    private List<String> openid;

    public List<String> getOpenid() {
        return openid;
    }

    public void setOpenid(List<String> openid) {
        this.openid = openid;
    }
}
