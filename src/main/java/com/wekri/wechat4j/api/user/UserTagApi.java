package com.wekri.wechat4j.api.user;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.user.bean.*;
import com.wekri.wechat4j.util.HttpUtils;

import java.util.List;

/**
 * 用户标签管理
 *
 * @author liuweiguo
 * @date 2019/4/23.
 */
public class UserTagApi {

    /**
     * 创建标签
     * http请求方式：POST
     */
    private static String POST_CREATE_TAG = "https://api.weixin.qq.com/cgi-bin/tags/create?access_token=";

    /**
     * 获取公众号已创建的标签
     * http请求方式：GET
     */
    private static String GET_TAG = "https://api.weixin.qq.com/cgi-bin/tags/get?access_token=";

    /**
     * 编辑标签
     * http请求方式：POST
     */
    private static String POST_UPDATE_TAG = "https://api.weixin.qq.com/cgi-bin/tags/update?access_token=";

    /**
     * 删除标签
     * http请求方式：POST
     */
    private static String POST_DELETE_TAG = "https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=";

    /**
     * 获取标签下粉丝列表
     * http请求方式：GET
     */
    private static String GET_TAG_USER = "https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=";

    /**
     * 批量为用户打标签
     * http请求方式：POST
     */
    private static String POST_TAG_BATCHTAGGING = "https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=";

    /**
     * 批量为用户取消标签
     * http请求方式：POST
     */
    private static String POST_TAG_BATCHUNTAGGING = "https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=";

    /**
     * 获取用户身上的标签列表
     * http请求方式：POST
     */
    private static String POST_TAG_GETIDLIST = "https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token=";

    /**
     * 创建标签
     *
     * @param accessToken
     * @param tagName     要创建的标签名
     * @return
     */
    public TagResult createTag(String accessToken, String tagName) {
        JSONObject tag = new JSONObject();
        JSONObject name = new JSONObject();
        name.put("name", tagName);
        tag.put("tag", name);
        return HttpUtils.postJson(POST_CREATE_TAG + accessToken, tag.toJSONString(), TagResult.class);
    }

    /**
     * 获取公众号已创建的标签
     *
     * @param accessToken
     * @return
     */
    public Tags getTags(String accessToken) {
        return HttpUtils.get(GET_TAG + accessToken, Tags.class);
    }

    /**
     * 编辑标签
     *
     * @param accessToken
     * @param tag         需要修改的tag对象，id未原id，name改为新名字
     * @return
     */
    public BaseMsg updateTag(String accessToken, Tag tag) {
        TagResult tagResult = new TagResult();
        tagResult.setTag(tag);
        return HttpUtils.postJson(POST_UPDATE_TAG + accessToken, JSONObject.toJSONString(tagResult), BaseMsg.class);
    }

    /**
     * 删除标签
     *
     * @param accessToken
     * @param tagId       标签ID
     * @return
     */
    public BaseMsg deleteTag(String accessToken, Integer tagId) {
        Tag tag = new Tag();
        tag.setId(tagId);

        TagResult tagResult = new TagResult();
        tagResult.setTag(tag);
        return HttpUtils.postJson(POST_DELETE_TAG + accessToken, JSONObject.toJSONString(tagResult), BaseMsg.class);
    }

    /**
     * 获取标签下粉丝列表
     *
     * @param accessToken
     * @param tagId       标签ID
     * @param nextOpenId  第一个拉取的OPENID，不填默认从头开始拉取
     */
    public TagUserResult getTagUser(String accessToken, Integer tagId, String nextOpenId) {
        JSONObject json = new JSONObject();
        json.put("tagid", tagId);
        json.put("next_openid", nextOpenId);
        return HttpUtils.postJson(GET_TAG_USER + accessToken, json.toJSONString(), TagUserResult.class);
    }

    /**
     * 批量为用户打标签
     * 注：每个粉丝身上的标签数最多20个
     *
     * @param accessToken
     * @param tagId       标签ID
     * @param openIdList  粉丝列表，每次传入的openid列表个数不能超过50个
     */
    public BaseMsg batchTagging(String accessToken, Integer tagId, List<String> openIdList) {
        JSONObject json = new JSONObject();
        json.put("tagid", tagId);
        json.put("openid_list", openIdList);
        return HttpUtils.postJson(POST_TAG_BATCHTAGGING + accessToken, json.toJSONString(), BaseMsg.class);
    }

    /**
     * 批量为用户取消标签
     *
     * @param accessToken
     * @param tagId       标签ID
     * @param openIdList  粉丝列表，每次传入的openid列表个数不能超过50个
     * @return
     */
    public BaseMsg batchUnTagging(String accessToken, Integer tagId, List<String> openIdList) {
        JSONObject json = new JSONObject();
        json.put("tagid", tagId);
        json.put("openid_list", openIdList);
        return HttpUtils.postJson(POST_TAG_BATCHUNTAGGING + accessToken, json.toJSONString(), BaseMsg.class);
    }

    /**
     * 获取用户身上的标签列表
     *
     * @param accessToken
     * @param openId      用户openid
     * @return
     */
    public UserTagResult getUserTag(String accessToken, String openId) {
        JSONObject json = new JSONObject();
        json.put("openid", openId);
        return HttpUtils.postJson(POST_TAG_GETIDLIST + accessToken, json.toJSONString(), UserTagResult.class);
    }
}
