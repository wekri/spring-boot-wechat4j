package com.wekri.wechat4j.api.user.bean;

import java.util.List;

/**
 * @author liuweiguo
 * @date 2019/1/16.
 */
public class UserListData {
    List<String> openid;

    public List<String> getOpenid() {
        return openid;
    }

    public void setOpenid(List<String> openid) {
        this.openid = openid;
    }
}
