package com.wekri.wechat4j.api.user.bean;

import com.wekri.wechat4j.BaseMsg;

import java.util.List;

/**
 * @author liuweiguo
 * @date 2019/4/23.
 */
public class Tags extends BaseMsg {
    List<Tag> tags;

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
