package com.wekri.wechat4j.api.promotion.bean;

import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.util.HttpUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URLEncoder;

/**
 * @author liuweiguo
 * @date 2018/11/6.
 */
public class QrCodeTicket extends BaseMsg {

    /**
     * 通过ticket换取二维码
     * TICKET记得进行UrlEncode
     */
    private static final String SHOW_QRCODE_GET = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=";

    /**
     * 获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
     */
    private String ticket;
    /**
     * 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天）。
     */
    private String expire_seconds;
    /**
     * 二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片
     */
    private String url;

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getExpire_seconds() {
        return expire_seconds;
    }

    public void setExpire_seconds(String expire_seconds) {
        this.expire_seconds = expire_seconds;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取二维码地址，可以直接展示
     *
     * @return 二维码地址
     */
    public String showQrCode() {
        return SHOW_QRCODE_GET + ticket;
    }

    /**
     * 下载二维码到指定目录
     *
     * @param path
     * @throws Exception
     */
    public void downloadQrCode(String path) throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
        IOUtils.write(downloadQrCode(), fileOutputStream);
        IOUtils.closeQuietly(fileOutputStream);
    }

    /**
     * 获取二维码byte
     *
     * @return
     * @throws Exception
     */
    public byte[] downloadQrCode() throws Exception {
        return HttpUtils.getFile(SHOW_QRCODE_GET + URLEncoder.encode(ticket, "UTF-8"));
    }
}
