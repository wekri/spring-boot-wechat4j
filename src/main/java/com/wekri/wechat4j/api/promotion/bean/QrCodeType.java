package com.wekri.wechat4j.api.promotion.bean;

/**
 * @author liuweiguo
 * @date 2018/11/6.
 */
public enum QrCodeType {
    /**
     * 临时的整型
     */
    QR_SCENE,
    /**
     * 临时的字符串
     */
    QR_STR_SCENE,

    /**
     * 永久的整型
     */
    QR_LIMIT_SCENE,
    /**
     * 永久的字符串
     */
    QR_LIMIT_STR_SCENE,;
}
