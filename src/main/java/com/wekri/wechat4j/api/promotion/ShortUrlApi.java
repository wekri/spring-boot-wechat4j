package com.wekri.wechat4j.api.promotion;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.promotion.bean.ShortUrl;
import com.wekri.wechat4j.util.HttpUtils;

/**
 * 长链接转短链接接口
 *
 * @author liuweiguo
 * @date 2018/11/6.
 */
public class ShortUrlApi {

    /**
     * http请求方式: POST
     */
    private static final String SHORT_URL_POST = "https://api.weixin.qq.com/cgi-bin/shorturl?access_token=";

    /**
     * 长链接转短链接
     *
     * @param longUrl     需要转换的长链接，支持http://、https://、weixin://wxpay 格式的url
     * @param accessToken 调用接口凭证
     * @return
     */
    public ShortUrl toShortUrl(String longUrl, String accessToken) {
        JSONObject json = new JSONObject();
        json.put("action", "long2short");
        json.put("long_url", longUrl);
        return HttpUtils.postJson(SHORT_URL_POST + accessToken, json.toJSONString(), ShortUrl.class);
    }

}
