package com.wekri.wechat4j.api.promotion;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.promotion.bean.QrCodeTicket;
import com.wekri.wechat4j.api.promotion.bean.QrCodeType;
import com.wekri.wechat4j.util.HttpUtils;

/**
 * 生成带参数的二维码
 * @author liuweiguo
 * @date 2018/11/6.
 */
public class QrCodeApi {

    private static final String CREATE_QRCODE_POST = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=";

    /**
     * 创建临时二维码
     *
     * @param sceneId       场景值ID，32位非0整型
     * @param expireSeconds 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天）
     * @param accessToken
     * @return
     */
    public QrCodeTicket createTemporaryQRCode(int sceneId, int expireSeconds, String accessToken) {
        return createQRCode(QrCodeType.QR_SCENE, sceneId, expireSeconds, accessToken);
    }

    /**
     * 创建临时二维码
     *
     * @param sceneId       场景值ID（字符串形式的ID），字符串类型，长度限制为1到64
     * @param expireSeconds 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天）
     * @param accessToken
     * @return
     */
    public QrCodeTicket createTemporaryQRCodeStr(String sceneId, int expireSeconds, String accessToken) {
        return createQRCode(QrCodeType.QR_STR_SCENE, sceneId, expireSeconds, accessToken);
    }

    /**
     * 创建永久二维码
     *
     * @param sceneId     场景ID，最大值为100000（目前参数只支持1--100000）
     * @param accessToken
     * @return
     */
    public QrCodeTicket createPermanentQRCode(int sceneId, String accessToken) {
        return createQRCode(QrCodeType.QR_LIMIT_SCENE, sceneId, null, accessToken);
    }

    /**
     * 创建永久二维码
     *
     * @param sceneId     场景值ID（字符串形式的ID），字符串类型，长度限制为1到64
     * @param accessToken
     * @return
     */
    public QrCodeTicket createPermanentQRCodeStr(String sceneId, String accessToken) {
        return createQRCode(QrCodeType.QR_LIMIT_STR_SCENE, sceneId, null, accessToken);
    }

    /**
     * 创建二维码ticket
     *
     * @param qrCodeType    二维码类型
     * @param sceneId       场景ID
     * @param expireSeconds 临时二维码有效时间，单位：秒
     * @param accessToken
     * @return
     */
    private QrCodeTicket createQRCode(QrCodeType qrCodeType, Object sceneId, Integer expireSeconds, String accessToken) {
        JSONObject scene = new JSONObject();
        if (sceneId instanceof String) {
            scene.put("scene_str", sceneId);
        } else {
            scene.put("scene_id", sceneId);
        }

        JSONObject action_info = new JSONObject();
        action_info.put("scene", scene);

        JSONObject json = new JSONObject();
        json.put("action_name", qrCodeType);
        json.put("action_info", action_info);
        if (qrCodeType == QrCodeType.QR_SCENE || qrCodeType == QrCodeType.QR_STR_SCENE) {
            json.put("expire_seconds", expireSeconds);
        }
        return HttpUtils.postJson(CREATE_QRCODE_POST + accessToken, json.toJSONString(), QrCodeTicket.class);
    }

}

