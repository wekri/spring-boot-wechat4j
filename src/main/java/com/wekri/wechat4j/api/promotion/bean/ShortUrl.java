package com.wekri.wechat4j.api.promotion.bean;

import com.wekri.wechat4j.BaseMsg;

/**
 * @author liuweiguo
 * @date 2018/11/6.
 */
public class ShortUrl extends BaseMsg{
    private String short_url;

    public String getShort_url() {
        return short_url;
    }

    public void setShort_url(String short_url) {
        this.short_url = short_url;
    }
}
