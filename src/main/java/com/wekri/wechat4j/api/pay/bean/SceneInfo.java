package com.wekri.wechat4j.api.pay.bean;

/**
 * @author liuweiguo
 * @date 2019/6/21.
 */
public class SceneInfo {

    /**
     * 门店id id 否 String(32) SZTX001 门店编号，由商户自定义
     */
    private String id;
    /**
     * 门店名称   否  String(64) 腾讯大厦腾大餐厅  门店名称 ，由商户自定义
     */
    private String name;
    /**
     * 门店行政区划码   否  String(6) 440305 门店所在地行政区划码，详细见《最新县及县以上行政区划代码》
     */
    private String area_code;
    /**
     * 门店详细地址   否  String(128) 科技园中一路腾讯大厦  门店详细地址 ，由商户自定义
     */
    private String address;

}
