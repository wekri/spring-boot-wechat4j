package com.wekri.wechat4j.api.oauth.bean;

/**
 * @author liuweiguo
 * @date 2019/12/11.
 */
public enum Scope {
    SNSAPI_BASE,
    SNSAPI_USERINFO;
}
