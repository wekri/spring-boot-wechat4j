package com.wekri.wechat4j.api.request;

/**
 * @author liuweiguo.
 */
public enum MsgType {
    event,

    /**
     * 文本消息
     * 1. 客服接口-发消息
     * 2. 接收普通消息:当普通微信用户向公众账号发消息时，微信服务器将POST消息的XML数据包到开发者填写的URL上。:https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140453
     */
    text,
    /**
     * 图片消息
     */
    image,
    /**
     * 语音消息
     */
    voice,
    /**
     * 视频消息
     */
    video,
    /**
     * 小视频消息
     */
    shortvideo,
    /**
     * 地理位置消息
     */
    location,
    /**
     * 链接消息
     */
    link,
    /**
     * 发送音乐消息
     */
    music,
    /**
     * 发送图文消息（点击跳转到外链） 图文消息条数限制在8条以内，注意，如果图文数超过8，则将会无响应。
     */
    news,
    /**
     * 发送图文消息（点击跳转到图文消息页面） 图文消息条数限制在8条以内，注意，如果图文数超过8，则将会无响应。
     */
    mpnews,
    /**
     * 发送菜单消息
     */
    msgmenu,
    /**
     * 发送卡券
     */
    wxcard,

    /**
     * 发送小程序卡片（要求小程序与公众号已关联）
     */
    miniprogrampage,;

}
