package com.wekri.wechat4j.api.request;

/**
 * @author liuweiguo.
 */
public enum Event {
    subscribe,//关注公众号事件推送
    unsubscribe,//取消关注
    SCAN,//扫描二维码
    LOCATION,//上报地理位置事件
    TEMPLATESENDJOBFINISH,//微信模板消息推送结果回调

    //自定义菜单事件推送:https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141016
    CLICK,//点击菜单拉取消息时的事件推送
    VIEW,//点击菜单跳转链接时的事件推送
    scancode_push,//扫码推事件的事件推送
    scancode_waitmsg,//扫码推事件且弹出“消息接收中”提示框的事件推送
    pic_sysphoto,//弹出系统拍照发图的事件推送
    pic_photo_or_album,//弹出拍照或者相册发图的事件推送
    pic_weixin,//弹出微信相册发图器的事件推送
    location_select,//弹出地理位置选择器的事件推送

    //扫一扫事件推送：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455872179
    user_scan_product,//打开商品主页事件推送
    user_scan_product_enter_session,//进入公众号事件推送
    user_scan_product_async,//地理位置信息异步推送
    user_scan_product_verify_action,//商品审核结果推送

    //微信认证事件推送:https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455785130
    qualification_verify_success,//资质认证成功（此时立即获得接口权限）
    qualification_verify_fail,//资质认证失败,
    naming_verify_success,//名称认证成功（即命名成功）
    naming_verify_fail,//名称认证失败（这时虽然客户端不打勾，但仍有接口权限）
    annual_renew,//年审通知
    verify_expired,//认证过期失效通知审通知

    /**
     * 门店审核事件推送
     */
    poi_check_notify,

    /**
     * 进入会话事件
     */
    user_enter_tempsession,
    ;
}
