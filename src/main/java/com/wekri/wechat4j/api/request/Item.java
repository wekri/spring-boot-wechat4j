package com.wekri.wechat4j.api.request;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by liuweiguo.
 */
public class Item {
    private String PicMd5Sum;

    @XmlElement(name="PicMd5Sum")
    public String getPicMd5Sum() {
        return PicMd5Sum;
    }
    public void setPicMd5Sum(String picMd5Sum) {
        PicMd5Sum = picMd5Sum;
    }

}
