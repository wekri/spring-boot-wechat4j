package com.wekri.wechat4j.api.message;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.message.bean.MsgMenu;
import com.wekri.wechat4j.api.request.MsgType;
import com.wekri.wechat4j.util.HttpUtils;

import java.util.List;

/**
 * 主动发送客服消息
 *
 * @author liuweiguo
 * @date 2018/12/18.
 */
public class CustomerMsgApi {

    private static final String CUSTOM_SEND_POST = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=";


    /**
     * 发送客服消息
     *
     * @param accessToken 调用接口凭证
     * @param json        JSON数据包
     * @return
     */
    public BaseMsg send(String accessToken, JSONObject json) {
        return HttpUtils.postJson(CUSTOM_SEND_POST + accessToken, json.toJSONString(), BaseMsg.class);
    }

    /**
     * 发送文本消息
     *
     * @param accessToken
     * @param toUser
     * @param text
     * @return
     */
    public BaseMsg sendText(String accessToken, String toUser, String text) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.text);
        JSONObject textJson = new JSONObject();
        textJson.put("content", text);
        json.put("text", textJson);
        return send(accessToken, json);
    }

    /**
     * 发送图片消息
     *
     * @param accessToken
     * @param toUser
     * @param mediaId
     * @return
     */
    public BaseMsg sendImage(String accessToken, String toUser, String mediaId) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.image);
        JSONObject data = new JSONObject();
        data.put("media_id", mediaId);
        json.put("image", data);
        return send(accessToken, json);
    }

    /**
     * 发送语音消息
     *
     * @param accessToken
     * @param toUser
     * @param mediaId
     * @return
     */
    public BaseMsg sendVoice(String accessToken, String toUser, String mediaId) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.voice);
        JSONObject data = new JSONObject();
        data.put("media_id", mediaId);
        json.put("voice", data);
        return send(accessToken, json);
    }

    /**
     * 发送视频消息
     *
     * @param accessToken
     * @param toUser
     * @param mediaId
     * @param title
     * @param description
     * @return
     */
    public BaseMsg sendVideo(String accessToken, String toUser, String mediaId,
                             String title, String description) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.video);
        JSONObject data = new JSONObject();
        data.put("media_id", mediaId);
        data.put("thumb_media_id", mediaId);
        data.put("title", title);
        data.put("description", description);
        json.put("video", data);
        return send(accessToken, json);
    }

    /**
     * 发送音乐消息
     *
     * @param accessToken
     * @param toUser
     * @param thumbMediaId
     * @param title
     * @param description
     * @param musicUrl
     * @param hqMusicUrl
     * @return
     */
    public BaseMsg sendMusic(String accessToken, String toUser, String thumbMediaId,
                             String title, String description, String musicUrl, String hqMusicUrl) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.music);
        JSONObject textJson = new JSONObject();
        textJson.put("title", title);
        textJson.put("description", description);
        textJson.put("musicurl", musicUrl);
        textJson.put("hqmusicurl", hqMusicUrl);
        textJson.put("thumb_media_id", thumbMediaId);
        json.put("music", textJson);
        return send(accessToken, json);
    }

    /**
     * 发送图文消息（点击跳转到外链） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。
     *
     * @param accessToken
     * @param toUser
     * @param title
     * @param description
     * @param url
     * @param picUrl
     * @return
     */
    public BaseMsg sendNews(String accessToken, String toUser, String title, String description,
                            String url, String picUrl) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.news);

        JSONArray arr = new JSONArray();

        JSONObject textJson = new JSONObject();
        textJson.put("title", title);
        textJson.put("description", description);
        textJson.put("url", url);
        textJson.put("picurl", picUrl);
        arr.add(textJson);

        JSONObject articles = new JSONObject();
        articles.put("articles", arr);

        json.put("news", articles);
        return send(accessToken, json);
    }


    /**
     * 发送图文消息（点击跳转到图文消息页面） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。
     *
     * @param accessToken
     * @param toUser
     * @param mediaId
     * @return
     */
    public BaseMsg sendMPNews(String accessToken, String toUser, String mediaId) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.mpnews);

        JSONObject textJson = new JSONObject();
        textJson.put("media_id", mediaId);
        json.put("mpnews", textJson);
        return send(accessToken, json);
    }

    /**
     * 发送卡券
     *
     * @param accessToken
     * @param toUser
     * @param cardId
     * @return
     */
    public BaseMsg sendWXCard(String accessToken, String toUser, String cardId) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.wxcard);

        JSONObject textJson = new JSONObject();
        textJson.put("card_id", cardId);
        json.put("wxcard", textJson);
        return send(accessToken, json);
    }

    /**
     * 发送菜单消息
     *
     * @param accessToken
     * @param toUser
     * @param head_content
     * @param tail_content
     * @param menuList
     * @return
     */
    public BaseMsg sendMsgMenu(String accessToken, String toUser,
                               String head_content, String tail_content, List<MsgMenu> menuList) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.msgmenu);

        JSONObject msnuJson = new JSONObject();
        msnuJson.put("head_content", head_content);
        msnuJson.put("tail_content", tail_content);
        msnuJson.put("list", menuList);

        json.put("msgmenu", msnuJson);
        return send(accessToken, json);
    }


    /**
     * 发送小程序卡片（要求小程序与公众号已关联）
     *
     * @param accessToken
     * @param toUser
     * @param title        小程序卡片的标题
     * @param appId        小程序的appid，要求小程序的appid需要与公众号有关联关系
     * @param pagePath     小程序的页面路径，跟app.json对齐，支持参数，比如pages/index/index?foo=bar
     * @param thumbMediaId 缩略图/小程序卡片图片的媒体ID，小程序卡片图片建议大小为520*416
     * @return
     */
    public BaseMsg sendMiniProgramPage(String accessToken, String toUser,
                                       String title, String appId, String pagePath, String thumbMediaId) {
        JSONObject json = new JSONObject();
        json.put("touser", toUser);
        json.put("msgtype", MsgType.miniprogrampage);

        JSONObject data = new JSONObject();
        data.put("title", title);
        data.put("appid", appId);
        data.put("pagepath", pagePath);
        data.put("thumb_media_id", thumbMediaId);

        json.put("miniprogrampage", data);
        return send(accessToken, json);
    }
}
