package com.wekri.wechat4j.api.message.bean;

import com.wekri.wechat4j.BaseMsg;

/**
 * @author liuweiguo.
 */
public class TemplateMessageResult extends BaseMsg {
    private String msgid;

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }
}
