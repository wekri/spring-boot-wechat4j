package com.wekri.wechat4j.api.message.bean;

/**
 * @author liuweiguo.
 */
public class TemplateData {

    /**
     * 模板内容
     */
    private String value;

    /**
     * 非必填
     * 模板内容字体颜色，不填默认为黑色
     */
    private String color;

    /**
     * 默认黑色
     *
     * @param value
     */
    public TemplateData(String value) {
        this.value = value;
    }

    public TemplateData(String value, String color) {
        this.value = value;
        this.color = color;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
