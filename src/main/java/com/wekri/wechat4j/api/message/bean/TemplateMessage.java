package com.wekri.wechat4j.api.message.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * 微信模板消息
 *
 * @author liuweiguo.
 */
public class TemplateMessage {

    /**
     * 必填
     * 接收者openid
     */
    private String touser;
    /**
     * 必填
     * 模板ID
     */
    private String template_id;
    /**
     * 非必填
     * 模板跳转链接
     */
    private String url;
    /**
     * 非必填
     * 跳小程序所需数据，不需跳小程序可不用传该数据
     */
    private MiniProgram miniprogram;
    /**
     * 必填
     * 模板数据
     */
    private Map<String, TemplateData> data;

    public TemplateMessage() {
    }

    public TemplateMessage(TemplateMessage.MessageBuilder messageBuilder) {
        this.touser = messageBuilder.touser;
        this.template_id = messageBuilder.template_id;
        this.url = messageBuilder.url;
        this.miniprogram = messageBuilder.miniprogram;
        this.data = messageBuilder.templateData;
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public MiniProgram getMiniprogram() {
        return miniprogram;
    }

    public void setMiniprogram(MiniProgram miniprogram) {
        this.miniprogram = miniprogram;
    }

    public Map<String, TemplateData> getData() {
        return data;
    }

    public void setData(Map<String, TemplateData> data) {
        this.data = data;
    }

    public static class MessageBuilder {
        private String touser;
        private String template_id;
        private String url;
        private MiniProgram miniprogram;
        private Map<String, TemplateData> templateData = new HashMap<String, TemplateData>();

        public TemplateMessage.MessageBuilder setTouser(String touser) {
            this.touser = touser;
            return this;
        }

        public TemplateMessage.MessageBuilder setTemplate_id(String template_id) {
            this.template_id = template_id;
            return this;
        }

        public TemplateMessage.MessageBuilder setUrl(String url) {
            this.url = url;
            return this;
        }

        public TemplateMessage.MessageBuilder setMiniprogram(MiniProgram miniprogram) {
            this.miniprogram = miniprogram;
            return this;
        }

        public TemplateMessage.MessageBuilder addTemplateData(String key, TemplateData data) {
            this.templateData.put(key, data);
            return this;
        }

        public TemplateMessage build() {
            return new TemplateMessage(this);
        }
    }
}
