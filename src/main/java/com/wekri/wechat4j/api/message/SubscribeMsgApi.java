package com.wekri.wechat4j.api.message;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 一次性订阅消息
 *
 * @author liuweiguo
 * @date 2018/12/18.
 */
public class SubscribeMsgApi {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String appId = "wx916c3723305ebefa";
        String templateId = "D56VRKXDRX1m9Ja450U-1SU9pGV7hSD5POoysI7ilMQ";
        String url = "https://www.huishuaka.com";

        StringBuilder sb = new StringBuilder("https://mp.weixin.qq.com/mp/subscribemsg?action=get_confirm&appid=");
        sb.append(appId)
                .append("&scene=").append(1000)
                .append("&template_id=").append(templateId)
                .append("&redirect_url=").append(URLEncoder.encode(url,"utf-8"))
                .append("&reserved=test#wechat_redirect ");
        System.out.println(sb);
    }
}
