package com.wekri.wechat4j.api.message.bean;

import java.io.Serializable;

/**
 * 发送菜单消息,menu对象
 *
 * @author liuweiguo
 */
public class MsgMenu implements Serializable {
    private String id;
    private String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
