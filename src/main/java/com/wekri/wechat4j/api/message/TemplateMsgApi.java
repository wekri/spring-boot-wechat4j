package com.wekri.wechat4j.api.message;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseApi;
import com.wekri.wechat4j.api.message.bean.TemplateMessage;
import com.wekri.wechat4j.api.message.bean.TemplateMessageResult;
import com.wekri.wechat4j.util.HttpUtils;

/**
 * 模板消息接口
 * @author liuweiguo.
 */
public class TemplateMsgApi extends BaseApi {

    /**
     * 发送模板消息 http请求方式: POST
     */
    private static final String TEMPLATE_SEND_POST = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";

    /**
     * 发送模板消息
     *
     * @param message
     * @param accessToken
     * @return
     */
    public TemplateMessageResult send(TemplateMessage message, String accessToken) {
        return HttpUtils.postJson(TEMPLATE_SEND_POST + accessToken, JSONObject.toJSONString(message), TemplateMessageResult.class);
    }

    public TemplateMessageResult send(String templateMessageString, String accessToken) {
        return HttpUtils.postJson(TEMPLATE_SEND_POST + accessToken, templateMessageString, TemplateMessageResult.class);
    }

}
