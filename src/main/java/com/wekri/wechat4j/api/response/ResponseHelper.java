package com.wekri.wechat4j.api.response;

import com.wekri.wechat4j.api.request.MsgType;
import com.wekri.wechat4j.api.request.WeChatRequest;
import com.wekri.wechat4j.api.response.bean.Articles;
import com.wekri.wechat4j.api.response.bean.MediaId;
import com.wekri.wechat4j.api.response.bean.Music;
import com.wekri.wechat4j.api.response.bean.Video;

/**
 * 被动回复用户消息
 *
 * @author liuweiguo
 * @date 2019/8/31.
 */
public class ResponseHelper {

    /**
     * 被动回复图片消息
     *
     * @param wechatRequest
     * @param mediaId       通过素材管理中的接口上传多媒体文件，得到的id。
     * @return
     */
    public static ImageResponse buildImageResponse(WeChatRequest wechatRequest, String mediaId) {
        ImageResponse response = new ImageResponse();
        response.setFromUserName(wechatRequest.getToUserName());
        response.setToUserName(wechatRequest.getFromUserName());
        response.setCreateTime(System.currentTimeMillis());
        response.setMsgType(MsgType.image.name());
        response.setImage(new MediaId(mediaId));
        return response;
    }

    /**
     * 被动回复语音消息
     *
     * @param wechatRequest
     * @param mediaId
     * @return
     */
    public static VoiceResponse buildVoiceResponse(WeChatRequest wechatRequest, String mediaId) {
        VoiceResponse response = new VoiceResponse();
        response.setFromUserName(wechatRequest.getToUserName());
        response.setToUserName(wechatRequest.getFromUserName());
        response.setCreateTime(System.currentTimeMillis());
        response.setMsgType(MsgType.voice.name());
        response.setVoice(new MediaId(mediaId));
        return response;
    }

    public static VideoResponse buildVideoResponse(WeChatRequest wechatRequest, String mediaId, String title, String description) {
        VideoResponse response = new VideoResponse();
        response.setFromUserName(wechatRequest.getToUserName());
        response.setToUserName(wechatRequest.getFromUserName());
        response.setCreateTime(System.currentTimeMillis());
        response.setMsgType(MsgType.video.name());
        response.setVideo(new Video(mediaId, title, description));
        return response;
    }

    public static MusicResponse buildMusicResponse(WeChatRequest wechatRequest,
                                                   String title, String description, String musicUrl, String hQMusicUrl, String thumbMediaId) {
        MusicResponse response = new MusicResponse();
        response.setFromUserName(wechatRequest.getToUserName());
        response.setToUserName(wechatRequest.getFromUserName());
        response.setCreateTime(System.currentTimeMillis());
        response.setMsgType(MsgType.music.name());
        response.setMusic(new Music(title, description, musicUrl, hQMusicUrl, thumbMediaId));
        return response;
    }

    public static NewsResponse buildNewsResponse(WeChatRequest wechatRequest, Articles articles) {
        NewsResponse response = new NewsResponse();
        response.setFromUserName(wechatRequest.getToUserName());
        response.setToUserName(wechatRequest.getFromUserName());
        response.setCreateTime(System.currentTimeMillis());
        response.setMsgType(MsgType.news.name());
        response.setArticleCount(articles.getItem().size());
        response.setArticles(articles);
        return response;
    }
}
