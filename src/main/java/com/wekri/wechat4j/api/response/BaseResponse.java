package com.wekri.wechat4j.api.response;

import com.wekri.wechat4j.util.CDATA;

import javax.xml.bind.annotation.*;

/**
 * @author liuweiguo.
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"ToUserName", "FromUserName", "CreateTime", "MsgType"})
public class BaseResponse {
    /**
     * 是	接收方帐号（收到的OpenID）
     */
    private String ToUserName;
    /**
     * 是	开发者微信号
     */
    private String FromUserName;
    /**
     * 是	消息创建时间 （整型）
     */
    private long CreateTime;
    /**
     * 是
     */
    @CDATA
    private String MsgType;

    public String getToUserName() {
        return ToUserName;
    }

    public void setToUserName(String toUserName) {
        ToUserName = toUserName;
    }

    public String getFromUserName() {
        return FromUserName;
    }

    public void setFromUserName(String fromUserName) {
        FromUserName = fromUserName;
    }

    public long getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(long createTime) {
        CreateTime = createTime;
    }

    public String getMsgType() {
        return MsgType;
    }

    public void setMsgType(String msgType) {
        MsgType = msgType;
    }
}
