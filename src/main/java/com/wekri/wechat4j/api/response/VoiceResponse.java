package com.wekri.wechat4j.api.response;

import com.wekri.wechat4j.api.response.bean.MediaId;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 语音消息
 *
 * @author liuweiguo.
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"Voice"})
public class VoiceResponse extends BaseResponse {

    private MediaId Voice;

    public MediaId getVoice() {
        return Voice;
    }

    public void setVoice(MediaId voice) {
        Voice = voice;
    }
}
