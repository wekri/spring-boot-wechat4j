package com.wekri.wechat4j.api.response.bean;

import com.wekri.wechat4j.util.CDATA;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author liuweiguo
 * @date 2019/8/31.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Video {
    @CDATA
    @XmlElement(name = "MediaId")
    private String mediaId;
    @CDATA
    @XmlElement(name = "Title")
    private String title;
    @CDATA
    @XmlElement(name = "Description")
    private String description;

    public Video(String mediaId, String title, String description) {
        this.mediaId = mediaId;
        this.title = title;
        this.description = description;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
