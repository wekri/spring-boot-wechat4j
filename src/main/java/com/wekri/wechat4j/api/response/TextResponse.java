package com.wekri.wechat4j.api.response;

import com.wekri.wechat4j.util.CDATA;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 被动回复文本消息
 *
 * @author liuweiguo.
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"Content"})
public class TextResponse extends BaseResponse {
    /**
     * 是	回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
     */
    @CDATA
    private String Content;

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }
}
