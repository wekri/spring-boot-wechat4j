package com.wekri.wechat4j.api.response.bean;

import com.wekri.wechat4j.util.CDATA;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author liuweiguo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class MediaId {

    @CDATA()
    @XmlElement(name = "MediaId")
    private String mediaId;

    public MediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
