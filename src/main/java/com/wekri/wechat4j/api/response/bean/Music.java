package com.wekri.wechat4j.api.response.bean;

import com.wekri.wechat4j.util.CDATA;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author liuweiguo
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Music {
    @CDATA
    @XmlElement(name = "Title")
    private String title;
    @CDATA
    @XmlElement(name = "Description")
    private String description;
    @CDATA
    @XmlElement(name = "MusicUrl")
    private String musicUrl;
    @CDATA
    @XmlElement(name = "HQMusicUrl")
    private String hQMusicUrl;
    @CDATA
    @XmlElement(name = "ThumbMediaId")
    private String thumbMediaId;

    public Music(String title, String description, String musicUrl, String hQMusicUrl, String thumbMediaId) {
        this.title = title;
        this.description = description;
        this.musicUrl = musicUrl;
        this.hQMusicUrl = hQMusicUrl;
        this.thumbMediaId = thumbMediaId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public String gethQMusicUrl() {
        return hQMusicUrl;
    }

    public void sethQMusicUrl(String hQMusicUrl) {
        this.hQMusicUrl = hQMusicUrl;
    }

    public String getThumbMediaId() {
        return thumbMediaId;
    }

    public void setThumbMediaId(String thumbMediaId) {
        this.thumbMediaId = thumbMediaId;
    }
}
