package com.wekri.wechat4j.api.response;

import com.wekri.wechat4j.api.response.bean.Articles;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 被动回复图文消息
 *
 * @author liuweiguo.
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class NewsResponse extends BaseResponse {
    @XmlElement(name = "ArticleCount")
    private Integer articleCount;
    @XmlElement(name = "Articles")
    private Articles articles;

    public Integer getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(Integer articleCount) {
        this.articleCount = articleCount;
    }

    public Articles getArticles() {
        return articles;
    }

    public void setArticles(Articles articles) {
        this.articles = articles;
    }
}
