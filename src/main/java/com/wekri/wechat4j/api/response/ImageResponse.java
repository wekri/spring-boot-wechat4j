package com.wekri.wechat4j.api.response;

import com.wekri.wechat4j.api.response.bean.MediaId;

import javax.xml.bind.annotation.*;

/**
 * 被动回复图片消息
 *
 * @author liuweiguo.
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class ImageResponse extends BaseResponse {

    @XmlElement(name = "Image")
    private MediaId image;

    public MediaId getImage() {
        return image;
    }

    public void setImage(MediaId image) {
        this.image = image;
    }
}
