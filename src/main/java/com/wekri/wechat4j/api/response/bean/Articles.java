package com.wekri.wechat4j.api.response.bean;

import java.util.List;

/**
 * @author liuweiguo
 */
public class Articles {
    List<ArticleItem> item;

    public List<ArticleItem> getItem() {
        return item;
    }

    public void setItem(List<ArticleItem> item) {
        this.item = item;
    }
}
