package com.wekri.wechat4j.api.menu;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.menu.bean.ConditionalResult;
import com.wekri.wechat4j.api.menu.bean.Menu;
import com.wekri.wechat4j.api.menu.bean.MenuResult;
import com.wekri.wechat4j.util.HttpUtils;

/**
 * 个性化菜单api
 *
 * @author liuweiguo
 * @date 2018/11/2.
 */
public class PersonalizedMenuApi {

    /**
     * 创建个性化菜单
     */
    private static final String ADD_CONDITIONAL_POST = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=";

    /**
     * 删除个性化菜单
     */
    private static final String DEL_CONDITIONAL_POST = "https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token=";

    /**
     * 测试个性化菜单匹配结果
     */
    private static final String TRY_MATCH_POST = "https://api.weixin.qq.com/cgi-bin/menu/trymatch?access_token=";

    /**
     * 创建个性化菜单
     *
     * @param menu
     * @param accessToken
     * @return
     */
    public ConditionalResult addConditional(Menu menu, String accessToken) {
        return HttpUtils.postJson(ADD_CONDITIONAL_POST + accessToken, JSONObject.toJSONString(menu), ConditionalResult.class);
    }

    /**
     * 删除个性化菜单
     *
     * @param menuId
     * @param accessToken
     * @return
     */
    public BaseMsg deleteConditional(String menuId, String accessToken) {
        JSONObject json = new JSONObject();
        json.put("menuid", menuId);
        return HttpUtils.postJson(DEL_CONDITIONAL_POST + accessToken, json.toJSONString(), BaseMsg.class);
    }

    /**
     * 测试个性化菜单匹配结果
     *
     * @param userId      用户openid或者微信号
     * @param accessToken 公众号api token
     * @return
     */
    public MenuResult tryMatch(String userId, String accessToken) {
        JSONObject json = new JSONObject();
        json.put("user_id", userId);
        return HttpUtils.postJson(TRY_MATCH_POST + accessToken, json.toJSONString(), MenuResult.class);
    }

}
