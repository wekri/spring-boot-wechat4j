package com.wekri.wechat4j.api.menu.bean;


import java.util.ArrayList;
import java.util.List;

/**
 * 菜单按钮
 *
 * 对于不同的菜单类型，value的值意义不同。官网上设置的自定义菜单： Text:保存文字到value； Img、voice：保存mediaID到value； Video：保存视频下载链接到value； News：保存图文消息到news_info，同时保存mediaID到value； View：保存链接到url。 使用API设置的自定义菜单： click、scancode_push、scancode_waitmsg、pic_sysphoto、pic_photo_or_album、 pic_weixin、location_select：保存值到key；view：保存链接到url
 *
 * @author liuweiguo.
 */
public class Button {

    /**
     * 必须
     * 菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
     */
    private ButtonType type;
    /**
     * 必须
     * 菜单标题，不超过16个字节，子菜单不超过60个字节
     */
    private String name;
    /**
     * click等点击类型必须
     * 菜单KEY值，用于消息接口推送，不超过128字节
     */
    private String key;

    /**
     * view、miniprogram类型必须
     * 网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时，不支持小程序的老版本客户端将打开本url。
     */
    private String url;

    /**
     * media_id类型和view_limited类型必须
     * 调用新增永久素材接口返回的合法media_id
     */
    private String media_id;

    /**
     * miniprogram类型必须
     * 小程序的appid（仅认证公众号可配置）
     */
    private String appid;

    /**
     * miniprogram类型必须
     * 小程序的页面路径
     */
    private String pagepath;

    /**
     * 非必须
     * 二级菜单数组，个数应为1~5个
     */
    private List<Button> sub_button;

    public Button() {
    }

    public Button(ButtonType type, String name, String url) {
        this.type = type;
        this.name = name;
        this.url = url;
    }

    protected Button(ButtonBuilder buttonBuilder) {
        this.type = buttonBuilder.type;
        this.name = buttonBuilder.name;
        this.key = buttonBuilder.key;
        this.url = buttonBuilder.url;
        this.media_id = buttonBuilder.media_id;
        this.appid = buttonBuilder.appid;
        this.pagepath = buttonBuilder.pagepath;
        this.sub_button = buttonBuilder.sub_button;
    }

    public ButtonType getType() {
        return type;
    }

    public void setType(ButtonType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPagepath() {
        return pagepath;
    }

    public void setPagepath(String pagepath) {
        this.pagepath = pagepath;
    }

    public List<Button> getSub_button() {
        return sub_button;
    }

    public void setSub_button(List<Button> sub_button) {
        this.sub_button = sub_button;
    }

    public static final class ButtonBuilder {
        private String name;
        private ButtonType type;
        private String key;
        private String url;
        private String media_id;
        private String appid;
        private String pagepath;
        private List<Button> sub_button = new ArrayList<>();

        public ButtonBuilder() {
        }

        public ButtonBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ButtonBuilder type(ButtonType type) {
            this.type = type;
            return this;
        }

        public ButtonBuilder key(String key) {
            this.key = key;
            return this;
        }

        public ButtonBuilder url(String url) {
            this.url = url;
            return this;
        }

        public ButtonBuilder media_id(String media_id) {
            this.media_id = media_id;
            return this;
        }

        public ButtonBuilder appid(String appid) {
            this.appid = appid;
            return this;
        }

        public ButtonBuilder pagepath(String pagepath) {
            this.pagepath = pagepath;
            return this;
        }

        public ButtonBuilder sub_button(Button button) {
            this.sub_button.add(button);
            return this;
        }

        public Button build() {
            return new Button(this);
        }
    }

    @Override
    public String toString() {
        return "Button{" +
                "type=" + type +
                ", name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", url='" + url + '\'' +
                ", media_id='" + media_id + '\'' +
                ", appid='" + appid + '\'' +
                ", pagepath='" + pagepath + '\'' +
                ", sub_button=" + sub_button +
                '}';
    }
}
