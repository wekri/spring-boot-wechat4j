package com.wekri.wechat4j.api.menu.bean;

import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.menu.bean.Menu;

import java.util.List;

/**
 * @author liuweiguo
 * @date 2018/11/2.
 */
public class MenuResult extends BaseMsg {

    private Menu menu;
    private List<Menu> conditionalmenu;

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public List<Menu> getConditionalmenu() {
        return conditionalmenu;
    }

    public void setConditionalmenu(List<Menu> conditionalmenu) {
        this.conditionalmenu = conditionalmenu;
    }

    @Override
    public String toString() {
        return "MenuResult{" +
                "menu=" + menu +
                ", conditionalmenu=" + conditionalmenu +
                "} " + super.toString();
    }
}
