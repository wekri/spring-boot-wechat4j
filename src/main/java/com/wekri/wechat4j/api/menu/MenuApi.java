package com.wekri.wechat4j.api.menu;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.BaseMsg;
import com.wekri.wechat4j.api.menu.bean.Menu;
import com.wekri.wechat4j.api.menu.bean.MenuResult;
import com.wekri.wechat4j.util.HttpUtils;


/**
 * 自定义菜单api
 *
 * @author liuweiguo.
 */
public class MenuApi {

    /**
     * 自定义菜单创建接口
     * http请求方式：POST
     */
    private static final String MENU_CREATE_POST = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";

    /**
     * 自定义菜单查询接口
     * http请求方式：GET
     */
    private static final String MENU_GET_GET = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=";
    /**
     * 自定义菜单删除接口
     * http请求方式：GET
     */
    private static final String MENU_DELETE_GET = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=";

    /**
     * 自定义菜单创建接口
     * <p>
     * <ul>1、自定义菜单最多包括3个一级菜单，每个一级菜单最多包含5个二级菜单。<ul/>
     * <ul>2、一级菜单最多4个汉字，二级菜单最多7个汉字，多出来的部分将会以“...”代替。<ul/>
     * <ul>3、创建自定义菜单后，菜单的刷新策略是，在用户进入公众号会话页或公众号profile页时，如果发现上一次拉取菜单的请求在5分钟以前，就会拉取一下菜单，如果菜单有更新，就会刷新客户端的菜单。测试时可以尝试取消关注公众账号后再次关注，则可以看到创建后的效果。<ul/>
     *
     * @param menu
     * @param accessToken
     * @return
     */
    public BaseMsg createMenu(Menu menu, String accessToken) {
        return createMenu(JSONObject.toJSONString(menu), accessToken);
    }

    /**
     * 自定义菜单创建接口
     *
     * @param jsonMenu
     * @param accessToken
     * @return
     */
    public BaseMsg createMenu(String jsonMenu, String accessToken) {
        return HttpUtils.postJson(MENU_CREATE_POST + accessToken, jsonMenu, BaseMsg.class);
    }

    /**
     * 自定义菜单查询接口
     *
     * @param accessToken
     * @return 对应创建接口，正确的Json返回结果
     */
    public MenuResult getMenu(String accessToken) {
        return HttpUtils.get(MENU_GET_GET + accessToken, MenuResult.class);
    }

    /**
     * 自定义菜单删除接口
     *
     * @return
     */
    public BaseMsg deleteMenu(String accessToken) {
        return HttpUtils.get(MENU_DELETE_GET + accessToken, BaseMsg.class);
    }
}
