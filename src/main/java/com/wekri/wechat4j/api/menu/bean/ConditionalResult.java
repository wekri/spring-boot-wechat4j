package com.wekri.wechat4j.api.menu.bean;

import com.wekri.wechat4j.BaseMsg;

/**
 * @author liuweiguo
 * @date 2018/11/2.
 */
public class ConditionalResult extends BaseMsg {

    private String menuid;

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }
}
