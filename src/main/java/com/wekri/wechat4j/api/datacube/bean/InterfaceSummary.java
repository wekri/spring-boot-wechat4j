package com.wekri.wechat4j.api.datacube.bean;

/**
 * @author liuweiguo
 */
public class InterfaceSummary {
    /**
     * 数据的日期 yyyy-MM-dd
     */
    private String ref_date;
    /**
     * 数据的小时
     */
    private String ref_hour;
    /**
     * 通过服务器配置地址获得消息后，被动回复用户消息的次数
     */
    private String callback_count;
    /**
     * 上述动作的失败次数
     */
    private String fail_count;
    /**
     * 总耗时，除以callback_count即为平均耗时
     */
    private String total_time_cost;
    /**
     * 最大耗时
     */
    private String max_time_cost;

    public String getRef_date() {
        return ref_date;
    }

    public void setRef_date(String ref_date) {
        this.ref_date = ref_date;
    }

    public String getRef_hour() {
        return ref_hour;
    }

    public void setRef_hour(String ref_hour) {
        this.ref_hour = ref_hour;
    }

    public String getCallback_count() {
        return callback_count;
    }

    public void setCallback_count(String callback_count) {
        this.callback_count = callback_count;
    }

    public String getFail_count() {
        return fail_count;
    }

    public void setFail_count(String fail_count) {
        this.fail_count = fail_count;
    }

    public String getTotal_time_cost() {
        return total_time_cost;
    }

    public void setTotal_time_cost(String total_time_cost) {
        this.total_time_cost = total_time_cost;
    }

    public String getMax_time_cost() {
        return max_time_cost;
    }

    public void setMax_time_cost(String max_time_cost) {
        this.max_time_cost = max_time_cost;
    }

    @Override
    public String toString() {
        return "InterfaceSummary{" +
                "ref_date='" + ref_date + '\'' +
                ", ref_hour='" + ref_hour + '\'' +
                ", callback_count='" + callback_count + '\'' +
                ", fail_count='" + fail_count + '\'' +
                ", total_time_cost='" + total_time_cost + '\'' +
                ", max_time_cost='" + max_time_cost + '\'' +
                '}';
    }
}
