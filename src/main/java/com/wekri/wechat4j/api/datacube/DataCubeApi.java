package com.wekri.wechat4j.api.datacube;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.api.datacube.bean.InterfaceSummary;
import com.wekri.wechat4j.util.HttpUtils;

import java.util.List;

/**
 * 1、接口侧的公众号数据的数据库中仅存储了2014年12月1日之后的数据，将查询不到在此之前的日期，即使有查到，也是不可信的脏数据；
 * 2、请开发者在调用接口获取数据后，将数据保存在自身数据库中，即加快下次用户的访问速度，也降低了微信侧接口调用的不必要损耗。
 * 3、为确保公众号数据已完成统计和处理，请于每天上午8点后查询公众号前一天的数据。
 *
 * @author liuweiguo
 */
public class DataCubeApi {
    /**
     * 获取接口分析数据
     */
    private String POST_INTERFACE_SUMMARY = "https://api.weixin.qq.com/datacube/getinterfacesummary?access_token=";
    /**
     * 获取接口分析分时数据
     */
    private String POST_INTERFACE_SUMMARY_HOUR = "https://api.weixin.qq.com/datacube/getinterfacesummaryhour?access_token=";

    /**
     * 获取接口分析数据，最大时间跨度30天
     *
     * @param access_token 调用接口凭证
     * @param begin_date   获取数据的起始日期，begin_date和end_date的差值需小于“最大时间跨度”（比如最大时间跨度为1时，begin_date和end_date的差值只能为0，才能小于1），否则会报错
     * @param end_date     获取数据的结束日期，end_date允许设置的最大值为昨日
     */
    public List<InterfaceSummary> interfaceSummary(String access_token, String begin_date, String end_date) {
        JSONObject param = new JSONObject();
        param.put("begin_date", begin_date);
        param.put("end_date", end_date);
        String str = HttpUtils.postJson(POST_INTERFACE_SUMMARY + access_token, param.toJSONString());
        JSONObject jsonObject = JSONObject.parseObject(str);
        return JSONArray.parseArray(jsonObject.getJSONArray("list").toJSONString(), InterfaceSummary.class);
    }

    /**
     * 获取接口分析数据，最大时间跨度1天
     *
     * @param access_token 调用接口凭证
     * @param begin_date   获取数据的起始日期，begin_date和end_date的差值需小于“最大时间跨度”（比如最大时间跨度为1时，begin_date和end_date的差值只能为0，才能小于1），否则会报错
     * @param end_date     获取数据的结束日期，end_date允许设置的最大值为昨日
     */
    public List<InterfaceSummary> interfaceSummaryHour(String access_token, String begin_date, String end_date) {
        JSONObject param = new JSONObject();
        param.put("begin_date", begin_date);
        param.put("end_date", end_date);
        String str = HttpUtils.postJson(POST_INTERFACE_SUMMARY_HOUR + access_token, param.toJSONString());
        JSONObject jsonObject = JSONObject.parseObject(str);
        return JSONArray.parseArray(jsonObject.getJSONArray("list").toJSONString(), InterfaceSummary.class);
    }

}
