package com.wekri.wechat4j.api.poi;

/**
 * @author liuweiguo
 * @date 2018/11/5.
 */
public class Business {
    private BaseInfo base_info;

    public Business() {
    }

    public Business(BaseInfo base_info) {
        this.base_info = base_info;
    }

    public BaseInfo getBase_info() {
        return base_info;
    }

    public void setBase_info(BaseInfo base_info) {
        this.base_info = base_info;
    }
}
