package com.wekri.wechat4j.api.poi;

import com.wekri.wechat4j.BaseMsg;

/**
 * @author liuweiguo
 * @date 2018/11/5.
 */
public class PoiResult extends BaseMsg {
    private String poi_id;

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }
}
