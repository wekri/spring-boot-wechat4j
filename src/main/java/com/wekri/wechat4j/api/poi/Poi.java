package com.wekri.wechat4j.api.poi;

/**
 * @author liuweiguo
 * @date 2018/11/5.
 */
public class Poi {
    private Business business;

    public Poi() {
    }

    public Poi(Business business) {
        this.business = business;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }
}
