package com.wekri.wechat4j.api.poi;

import com.alibaba.fastjson.JSONObject;
import com.wekri.wechat4j.util.HttpUtils;

/**
 * @author liuweiguo
 * @date 2018/11/5.
 */
public class PoiApi {

    private static final String ADD_POI_POST = "http://api.weixin.qq.com/cgi-bin/poi/addpoi?access_token=";
    /**
     * 查询门店信息
     * http请求方式	POST
     */
    private static final String GET_POI_POST = "http://api.weixin.qq.com/cgi-bin/poi/getpoi?access_token=";

    /**
     * 查询门店列表
     * http请求方式	POST
     */
    private static final String GET_POI_LIST_POST = "https://api.weixin.qq.com/cgi-bin/poi/getpoilist?access_token=";

    /**
     * 修改门店服务信息
     * http请求方式	POST/FROM
     */
    private static final String UPDATE_POI_POST = "https://api.weixin.qq.com/cgi-bin/poi/updatepoi?access_token=";

    /**
     * 删除门店
     * http请求方式	POST/FROM
     */
    private static final String DELETE_POI_POST = "https://api.weixin.qq.com/cgi-bin/poi/delpoi?access_token=";

    /**
     * 门店类目表
     * http请求方式	GET
     */
    private static final String GET_WX_CATEGORY_GET = "http://api.weixin.qq.com/cgi-bin/poi/getwxcategory?access_token=";

    public PoiResult addPoi(Poi poi, String accessToken) {
        return HttpUtils.postJson(ADD_POI_POST + accessToken, JSONObject.toJSONString(poi), PoiResult.class);
    }

    public Poi getPoi(String poiId, String accessToken) {
        JSONObject json = new JSONObject();
        json.put("poi_id", poiId);
        return HttpUtils.postJson(GET_POI_POST + accessToken, json.toJSONString(), Poi.class);
    }
}
